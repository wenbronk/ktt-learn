//package com.wenbronk.security.simple.security.userDetails;
//
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//
//import org.springframework.security.access.AccessDeniedException;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * @Author wenbronk
// * @Date 2019/11/1 4:27 下午
// * description:
// */
//public class UserDetailsREepository {
//
//    private Map<String, UserDetails> users = new HashMap<>();
//
//    public void createUser(UserDetails user) {
//        users.putIfAbsent(user.getUsername(), user);
//    }
//
//    public void updateUser(UserDetails user) {
//        users.put(user.getUsername(), user);
//    }
//
//    public void deleteUser(String username) {
//        users.remove(username);
//    }
//
//    public void changePassword(String oldPassword, String newPassword) {
//        Authentication currentUser = SecurityContextHolder.getContext()
//                .getAuthentication();
//
//        if (currentUser == null) {
//            // This would indicate bad coding somewhere
//            throw new AccessDeniedException("Can't change password as no Authentication object found in context "
//                            + "for current user.");
//        }
//
//        String username = currentUser.getName();
//
//        UserDetails user = users.get(username);
//
//
//        if (user == null) {
//            throw new IllegalStateException("Current user doesn't exist in database.");
//        }
//    }
//
//    public boolean userExists(String username) {
//        return users.containsKey(username);
//    }
//
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return users.get(username);
//    }
//}
