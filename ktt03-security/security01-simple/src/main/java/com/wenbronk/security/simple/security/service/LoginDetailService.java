package com.wenbronk.security.simple.security.service;

import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.repository.UserRepository;
import com.wenbronk.security.simple.security.model.SysUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @Author wenbronk
 * @Date 2019/10/30 10:48 下午
 * description:
 */
@Service
public class LoginDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = userRepository.findByUsername(username);
        if (sysUser == null) throw new RuntimeException("用户名不存在");

        SysUserDetails sysUserDetails = new SysUserDetails(sysUser);
        return sysUserDetails;
    }
}
