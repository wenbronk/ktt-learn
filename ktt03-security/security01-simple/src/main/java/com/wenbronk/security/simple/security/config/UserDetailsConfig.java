//package com.wenbronk.security.simple.security.config;
//
//import com.wenbronk.security.simple.security.userDetails.CusUserDetailManager;
//import com.wenbronk.security.simple.security.userDetails.UserDetailsREepository;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//
///**
// * @Author wenbronk
// * @Date 2019/11/1 4:30 下午
// * description: userDetails验证相关的bean
// */
//@Configuration
//public class UserDetailsConfig {
//
//    @Bean
//    public UserDetailsREepository userDetailsREepository() {
//        UserDetailsREepository userDetailsREepository = new UserDetailsREepository();
//
//        // authorities 一定不能为null 这代表用户的角色权限集合
//        UserDetails userDetails = User.withUsername("admin").password("{noop}admin").authorities(AuthorityUtils.NO_AUTHORITIES).build();
//        userDetailsREepository.createUser(userDetails);
//        return userDetailsREepository;
//    }
//
//    @Bean
//    public CusUserDetailManager userDetailManager() {
//        return new CusUserDetailManager();
//    }
//}
