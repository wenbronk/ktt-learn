package com.wenbronk.security.simple.security.verify;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author wenbronk
 * @Date 2019/10/31 7:47 下午
 * description: 默认只校验用户名密码， 这儿可以加入验证码之类
 */

public class CustomWebAuthenticationDetails extends WebAuthenticationDetails {
//    private final String verifyCode;

    public CustomWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
//        verifyCode = request.getParameter("verifyCode");
    }

//    public String getVerifyCode() {
//        return verifyCode;
//    }

//    @Override
//    public String toString() {
//        StringBuilder sb = new StringBuilder();
//        sb.append(super.toString()).append("; verifyCode: ").append(this.getVerifyCode());
//        return sb.toString();
//    }
}
