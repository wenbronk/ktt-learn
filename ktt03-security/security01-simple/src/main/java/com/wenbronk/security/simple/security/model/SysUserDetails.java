package com.wenbronk.security.simple.security.model;

import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.model.SysUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @Author wenbronk
 * @Date 2019/10/31 7:35 下午
 * description:
 */
@Data
public class SysUserDetails extends SysUser implements UserDetails {

    public SysUserDetails(SysUser sysUser) {
        super(sysUser.getId(), sysUser.getPhone(), sysUser.getUsername(), sysUser.getPassword(), sysUser.getRoles());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> auths = new ArrayList<>();
        Set<SysRole> roles = this.getRoles();
        for (SysRole role : roles) {
            auths.add(new SimpleGrantedAuthority(role.getRoleName()));

            for (SysPermission permission : role.getPermissions()) {
                auths.add(new SimpleGrantedAuthority(permission.getName()));
            }

        }
        return auths;

    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
