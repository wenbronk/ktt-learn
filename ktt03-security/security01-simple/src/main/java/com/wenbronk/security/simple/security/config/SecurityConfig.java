package com.wenbronk.security.simple.security.config;

import com.wenbronk.security.simple.security.handler.FailedHandler;
import com.wenbronk.security.simple.security.handler.LoginoutHandler;
import com.wenbronk.security.simple.security.handler.SuccessHandler;
import com.wenbronk.security.simple.security.service.LoginDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @Author wenbronk
 * @Date 2019/10/30 10:35 下午
 * description:
 */
@Configuration
@EnableWebSecurity(debug = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private LoginDetailService loginDetailService;

//    @Autowired
//    private CustomAuthenticationProvider customAuthenticationProvider;

    // 加密类
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 去除角色前缀 ROLE_ 的
     * @return
     */
    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults(""); // Remove the ROLE_ prefix
    }


    /**
     * 认证管理, 与userDtailes相关的
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                // 加入验证码的校验类
                // .authenticationProvider(customAuthenticationProvider)
                // 设置UserDetailsService
                .userDetailsService(loginDetailService)
                // 使用BCrypt进行密码的hash
                .passwordEncoder(passwordEncoder());
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
        auth.eraseCredentials(false);
    }

    /**
     * 需要忽略的静态资源等
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().and()    //开启跨域
                .csrf().disable() //禁用csrf
                .headers().frameOptions().disable(); //禁用frame options
        http.authorizeRequests()
                .antMatchers("/home", "/about", "/img/*").permitAll()    // 放行
                .antMatchers("/order/**").hasAnyRole("USER", "ADMIN")    // 角色
                .anyRequest().authenticated();   // 登陆
        http.formLogin()
                .usernameParameter("username") // default is username
                .passwordParameter("password") // default is password
//                .loginPage("")
                .loginProcessingUrl("/sys/login")
//            .successForwardUrl("/sys/success")
//            .failureForwardUrl("/sys/failure")
//                .logoutUrl("sys/logout")
                .successHandler(successHandler())
                .failureHandler(failedHandler())
//                .authenticationDetailsSource(authenticationDetailsSource) // 自定义的登陆验证
                .and()
                .logout()
                .permitAll()
                // 登出前调用， 否则信息可能不存在， 可用于日志
//                .addLogoutHandler(logoutHandler())
                .logoutSuccessHandler(logoutHandler());

    }

    @Bean
    public AuthenticationSuccessHandler successHandler() {
        return new SuccessHandler();
    }
    @Bean
    public AuthenticationFailureHandler failedHandler() {
        return new FailedHandler();
    }
    @Bean
    public LogoutSuccessHandler logoutHandler() {
        return new LoginoutHandler();
    }

}
