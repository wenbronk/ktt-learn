package com.wenbronk.security.simple.security.verify;

import com.wenbronk.security.simple.security.service.LoginDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @Author wenbronk
 * @Date 2019/10/31 7:54 下午
 * description: 自定义认证器
 * https://www.cnblogs.com/hello-shf/p/10800457.html
 */
public class CustomAuthenticationProvider extends DaoAuthenticationProvider {

    @Autowired
    private LoginDetailService loginDetailService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        //用户输入的用户名
        String username = authentication.getName();
        //用户输入的密码
        String password = authentication.getCredentials().toString();
        //通过CustomWebAuthenticationDetails获取用户输入的验证码信息
        CustomWebAuthenticationDetails details = (CustomWebAuthenticationDetails) authentication.getDetails();
//        String verifyCode = details.getVerifyCode();
//        if(null == verifyCode || verifyCode.isEmpty()){
//            log.warn("未输入验证码");
//            throw new NullPointerException("请输入验证码");
//        }
//        //校验验证码
//        if(!validateVerifyCode(verifyCode)){
//            log.warn("验证码输入错误");
//            throw new DisabledException("验证码输入错误");
//        }
        //通过自定义的CustomUserDetailsService，以用户输入的用户名查询用户信息
        UserDetails userDetails = (UserDetails) loginDetailService.loadUserByUsername(username);
        //校验用户密码
        if(!userDetails.getPassword().equals(password)){
            throw new BadCredentialsException("密码错误");
        }
//        Object principalToReturn = userDetails;
//        //将用户信息塞到SecurityContext中，方便获取当前用户信息
//        return this.createSuccessAuthentication(principalToReturn, authentication, userDetails);
        return new UsernamePasswordAuthenticationToken(username, password, userDetails.getAuthorities());
    }

    /**
     * 是否使用
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
