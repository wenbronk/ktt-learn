package com.wenbronk.security.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk
 * @Date 2019/10/31 11:02 下午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security.simple"})
public class SecuritySimpleMain {
    public static void main(String[] args) {
        SpringApplication.run(SecuritySimpleMain.class, args);
    }
}
