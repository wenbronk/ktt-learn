package com.wenbronk.security.simple.security;

/**
 * @Author wenbronk
 * @Date 2019/10/31 10:35 上午
 * description:
 */
public enum LoginType {

    /**
     */
    FORM,
    PHONE;

    public static LoginType getLoginType(String type) {
        if ("form".equalsIgnoreCase(type)) {
            return FORM;
        } else {
            return PHONE;
        }
    }

}
