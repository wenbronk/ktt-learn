//package com.wenbronk.security.simple.security.userDetails;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.provisioning.UserDetailsManager;
//
///**
// * @Author wenbronk
// * @Date 2019/11/1 4:33 下午
// * description:
// */
//public class CusUserDetailManager implements UserDetailsManager {
//
//    @Autowired
//    private UserDetailsREepository userDetailsREepository;
//
//    @Override
//    public void createUser(UserDetails user) {
//        userDetailsREepository.createUser(user);
//    }
//
//    @Override
//    public void updateUser(UserDetails user) {
//        userDetailsREepository.updateUser(user);
//    }
//
//    @Override
//    public void deleteUser(String username) {
//        userDetailsREepository.deleteUser(username);
//    }
//
//    @Override
//    public void changePassword(String oldPassword, String newPassword) {
//        userDetailsREepository.changePassword(oldPassword, newPassword);
//    }
//
//    @Override
//    public boolean userExists(String username) {
//        return userDetailsREepository.userExists(username);
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return userDetailsREepository.loadUserByUsername(username);
//    }
//}
