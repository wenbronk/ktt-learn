package com.wenbronk.test.service;

import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.base.service.PermissionService;
import com.wenbronk.security.simple.SecuritySimpleMain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author wenbronk
 * @Date 2019/10/25 10:43 上午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecuritySimpleMain.class})
public class PermissionTest {

    @Autowired
    private PermissionService permissionService;

    @Test
    public void testAdd() {
        add("用户添加", "/user/addUser");
        add("用户删除", "/user/deleteUser");
        add("admin 权限", "/user/admin");
        add("staff 权限", "/user/staff");
    }

    public void add(String permissionName, String url) {
        SysPermission permission = new SysPermission();
        permission.setId(Utils.getId());
        permission.setName(permissionName);
        permission.setApiUrl(url);
        permissionService.save(permission);
    }

}
