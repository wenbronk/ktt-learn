package com.wenbronk.test.service;

import com.google.common.collect.Lists;

import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.service.RoleService;
import com.wenbronk.security.base.service.UserService;
import com.wenbronk.security.simple.SecuritySimpleMain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:30 下午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecuritySimpleMain.class})
public class UserTest {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Test
    public void testSave() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        SysUser user = new SysUser();
        user.setId(Utils.getId());
        user.setUsername("root");
        user.setPassword(bCryptPasswordEncoder.encode("root"));
        user.setPhone("110");
        userService.save(user);

        SysUser user1 = new SysUser();
        user1.setId(Utils.getId());
        user1.setUsername("admin");
        user1.setPassword(bCryptPasswordEncoder.encode("admin"));
        user1.setPhone("111");
        userService.save(user1);

        SysUser user2 = new SysUser();
        user2.setId(Utils.getId());
        user2.setUsername("staff");
        user2.setPassword(bCryptPasswordEncoder.encode("admin"));
        user2.setPhone("112");
        userService.save(user2);
    }

    @Test
    public void testAssignRootRole() {
        SysUser root = userService.findByUserName("root");
        String userId = root.getId();

        SysRole rootRole = roleService.findByRoleName("root");
        SysRole adminRole = roleService.findByRoleName("admin");
        SysRole staffRole = roleService.findByRoleName("staff");

        ArrayList<String> rolesIds = Lists.newArrayList(
                rootRole.getId(),
                adminRole.getId(),
                staffRole.getId());
        userService.assignRole2User(userId, rolesIds);
    }

    @Test
    public void testAssignAdminRole2() {
        SysUser root = userService.findByUserName("admin");
        String userId = root.getId();

        SysRole adminRole = roleService.findByRoleName("admin");
        SysRole staffRole = roleService.findByRoleName("staff");

        ArrayList<String> rolesIds = Lists.newArrayList(
                adminRole.getId(),
                staffRole.getId());
        userService.assignRole2User(userId, rolesIds);
    }

    @Test
    public void testAssignStaffRole3() {
        SysUser root = userService.findByUserName("staff");
        String userId = root.getId();

        SysRole staffRole = roleService.findByRoleName("staff");

        ArrayList<String> rolesIds = Lists.newArrayList(
                staffRole.getId());
        userService.assignRole2User(userId, rolesIds);
    }

}
