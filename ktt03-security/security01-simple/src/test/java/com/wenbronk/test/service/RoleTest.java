package com.wenbronk.test.service;

import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.service.PermissionService;
import com.wenbronk.security.base.service.RoleService;
import com.wenbronk.security.simple.SecuritySimpleMain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:43 下午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecuritySimpleMain.class})
public class RoleTest {

    @Autowired
    private RoleService roleService;

    @Autowired
    private PermissionService permissionService;

    @Test
    public void save() {
        SysRole role = new SysRole();
        role.setId(Utils.getId());
        role.setRoleName("root");
        roleService.save(role);

        SysRole role1 = new SysRole();
        role1.setId(Utils.getId());
        role1.setRoleName("admin");
        roleService.save(role1);

        SysRole role2 = new SysRole();
        role2.setId(Utils.getId());
        role2.setRoleName("staff");
        roleService.save(role2);
    }

    @Test
    public void testAsingPerm2Role() {
        SysPermission addPermission = permissionService.findByName("用户添加");
        SysPermission delPermission = permissionService.findByName("用户删除");
        SysPermission adminPermission = permissionService.findByName("admin 权限");
        SysPermission staffPermission = permissionService.findByName("staff 权限");

        asPerm2Role("root",
                addPermission.getId(),
                delPermission.getId(),
                adminPermission.getId(),
                staffPermission.getId()
                );

        asPerm2Role("admin",
                adminPermission.getId());

        asPerm2Role("staff",
                staffPermission.getId());

    }

    public void asPerm2Role(String roleName, String... permissinIds) {
        SysRole role = roleService.findByRoleName(roleName);
        Set<SysPermission> permissions = Stream.of(permissinIds).map(permissionService::findById).collect(Collectors.toSet());
        role.setPermissions(permissions);
        roleService.save(role);
    }

}
