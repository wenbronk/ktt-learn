package com.wenbronk.test;

import com.wenbronk.security.simple.SecuritySimpleMain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.*;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/26 2:28 下午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecuritySimpleMain.class})
public class DataSourceTest {

    @Autowired
    private DataSource dataSource;

    @Value("${spring.datasource.url}")
    private String url;

    @Test
    public void testAutowired() {
        System.err.println(url);
    }

    @Test
    public void testQueryDatabase() throws SQLException {
        DatabaseMetaData metaData = dataSource.getConnection().getMetaData();
        String url = metaData.getURL();
        System.err.println(url);
    }

    @Test
    public void testQueryTable() throws SQLException {
//        DatabaseMetaData metaData = dataSource.getConnection().getMetaData();
//        ResultSet rs = metaData.getTables(null, null, null, new String[]{"TABLE"});
//        while (rs.next()) {
//            System.err.println(rs.getString("TABLE_NAME"));
//        }

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("show tables;");

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            String string = resultSet.getString(0);
            System.out.println(string);
        }
    }

}
