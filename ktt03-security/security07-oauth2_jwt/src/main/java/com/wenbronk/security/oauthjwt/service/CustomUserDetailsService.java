package com.wenbronk.security.oauthjwt.service;

import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.repository.UserRepository;
import com.wenbronk.security.oauthjwt.model.UserJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/11 5:10 下午
 * description:
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = userRepository.findByUsername(username);
        List<SimpleGrantedAuthority> authorities = sysUser.getRoles().stream().flatMap(role ->
                role.getPermissions().stream().map(SysPermission::getName)
                        .map(SimpleGrantedAuthority::new)
        ).collect(Collectors.toList());

        authorities.addAll(
                sysUser.getRoles().stream().map(SysRole::getRoleName)
                        .map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

        UserDetails userDetails = User.withUsername(sysUser.getUsername())
                .password(sysUser.getPassword())
                .authorities(authorities)
                .build();

        UserJwt userJwt = new UserJwt(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
        userJwt.setCompanyId("asgdsafsafsadfsad");
        return userJwt;
    }
}
