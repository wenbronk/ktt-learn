package com.wenbronk.security.oauthjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/13 9:28 上午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security.oauthjwt"})
public class JwtOauthMain {
    public static void main(String[] args) {
        SpringApplication.run(JwtOauthMain.class, args);
    }
}
