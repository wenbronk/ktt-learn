package com.wenbronk.security.oauthjwt.config;

import com.wenbronk.security.oauthjwt.model.UserJwt;
import com.wenbronk.security.oauthjwt.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/13 5:29 下午
 * description: 往jwt中添加自定义信息
 */
@Component
public class CustomUserTokenConvert extends DefaultUserAuthenticationConverter {

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        LinkedHashMap<String, Object> response = new LinkedHashMap<>();
        String name = authentication.getName();

        Object principal = authentication.getPrincipal();
        UserJwt userJwt = null;
        if (principal instanceof UserJwt) {
            userJwt = (UserJwt) principal;
        } else {
            // refresh token 默认不调用， 需要手动调用
            userJwt = (UserJwt) userDetailsService.loadUserByUsername(name);
        }

        response.put("user_name", name);
        response.put("id", userJwt.getId());
        response.put("utype", userJwt.getUtype());
        if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
            response.put("authories", AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
        }
        return response;
    }
}
