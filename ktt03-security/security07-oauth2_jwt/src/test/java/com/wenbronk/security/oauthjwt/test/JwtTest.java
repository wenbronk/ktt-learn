package com.wenbronk.security.oauthjwt.test;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.util.HashMap;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/14 3:01 下午
 * description:
 */
public class JwtTest {

    @Test
    public void testDecode() {
        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ8.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbImFwcCJdLCJ1dHlwZSI6bnVsbCwiYXV0aG9yaWVzIjpbImFkbWluIiwic3RhZmYiLCJzdGFmZiDmnYPpmZAiLCJhZG1pbiDmnYPpmZAiXSwiaWQiOm51bGwsImV4cCI6MTU3MzgyNTgzNCwianRpIjoiNTA4YzU5NTktYzYyMS00ZjQ4LWExNGQtMDM4NzAyNTgxOWU3IiwiY2xpZW50X2lkIjoiY2xpZW50In0.2s6XwVoz4uZZq31RdaDFb8x1B4MEta6adM_-sqXbbtw";
        Jwt jwt = JwtHelper.decode(token);
        String claims = jwt.getClaims();

        String encoded = jwt.getEncoded();
        System.out.println(claims);
        System.out.println("===");
        System.out.println(encoded);
    }

    /**
     * 利用私钥生成jwt
     */
    @Test
    public void createSingerJwt() {
        String key = "wenbronk.jks";
        String keyPasswdStore = "a75767626store";
        ClassPathResource resource = new ClassPathResource(key);

        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource, keyPasswdStore.toCharArray());

        String alias = "wenbronk";
        String keyPasswd = "a75767626";
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair(alias, keyPasswd.toCharArray());

        RSAPrivateKey aPrivate = (RSAPrivateKey) keyPair.getPrivate();

        // 生成jwt
        JSONObject hashMap = new JSONObject();
        hashMap.put("username", "wenbronk");
        hashMap.put("companyId", "unkonw");
        hashMap.put("ext", "123424");

        Jwt jwt = JwtHelper.encode(hashMap.toString(), new RsaSigner(aPrivate));
        String encoded = jwt.getEncoded();
        System.out.println("encode:" + encoded);
    }

    /**
     * 利用公钥验证
     */
    String publicKey = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzrXlX+8JNAJCWe/KVXvE\n" +
            "56aQzAUcqYViuOa6mfhJzyjNR8YacCCzh0eSVmSAe1BMuKdv1r/+mVrm3a5qiJ3F\n" +
            "LzzUfZmV3lXOXhsaSDhiIqsU5BzLfOgfdmAG7ZwSp1XEa0G9xj4dF071zktzEi7z\n" +
            "br4faX3IAeUqZBNug4SM0aiB+il1suDCB1B2kfDuHXlKer166DupgCTQ3cT47K0m\n" +
            "yOIvQ9ecdzehZ1QgOT/6Zzsbzt0JPQt45CwBf2L7aRQyFdIm2ezg3gTi3E0CglaL\n" +
            "mx8qQKcMZOKXuPSXvwRnr5HU8qqS+1CY5myeUrsd9J9neYJxN8xUSbjN98UYqkzY\n" +
            "9wIDAQAB\n" +
            "-----END PUBLIC KEY-----";

    @Test
    public void testVerify() {
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHQiOiIxMjM0MjQiLCJjb21wYW55SWQiOiJ1bmtvbnciLCJ1c2VybmFtZSI6IndlbmJyb25rIn0.cqrgAcwI5bvLT0ZjWJxeEV_6jg632QOvXUkRvBE709P3ApoDXMOwBCgLO_b68ZzVf_HK0zCCFOVdd8IoI114t8mLI3a_6u0kPsnm0oB9eqmwVi709ejQEybpC0UY1u_hc1O2I6o4MktNaP1ipUJzTAIXMJRSNXIi8R6nlyLYtbc6j5EZf3i6utP2aE68kcv_bIsu2g4ZEl5xim51PlgS8f4r1xKn7UfcM21xC9tdB2hraIV6D0YuCF24XAtFf0UQrvjdkl2vKZytzAkohFCqWfn_wRKY5TgLzNqmNiqIR5qN2l1Qzwfou7iYIzq2ECYWfWs6xkBG2a8ZVN9oHuaoFA";
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publicKey));
//        Jwt jwt = JwtHelper.decode(token);
        String claims = jwt.getClaims();
        System.out.println("claim:" + claims);
    }

}
