dependencies {
    implementation(project(":ktt03-security:security00-base"))
    implementation("mysql:mysql-connector-java")

    implementation("org.springframework.security:spring-security-jwt:1.0.11.RELEASE")
    implementation("io.jsonwebtoken:jjwt:0.9.0")

}