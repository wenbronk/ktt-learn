import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/7 4:28 下午
 * description:
 */
public class PasswordTest {

    @Test
    public void testPasswd() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String staff = bCryptPasswordEncoder.encode("root");
        System.out.println(staff);
    }

    @Test
    public void testMatch() {
        boolean staff = new BCryptPasswordEncoder().matches("staff", "$2a$10$N7fnYpDH8tbFOMak.nO3gusJPHOObBq.LDTqLtNKV7TlvkTS194tm");
        System.out.println(staff);
    }

}
