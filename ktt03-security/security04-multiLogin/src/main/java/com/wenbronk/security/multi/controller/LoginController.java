package com.wenbronk.security.multi.controller;

import com.wenbronk.security.multi.token.MobilAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/7 10:57 上午
 * description:
 */
@RestController
@RequestMapping("/sys")
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @RequestMapping("/username")
    public String login(String username, String password) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authenticate = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        return "用户名密码登陆成功";
    }

    @RequestMapping("/mobile")
    public String login2(String mobile, String certify) {
        MobilAuthenticationToken mobilAuthenticationToken = new MobilAuthenticationToken(mobile, certify);
        Authentication authenticate = authenticationManager.authenticate(mobilAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        return "手机号验证码登陆成功";
    }

}
