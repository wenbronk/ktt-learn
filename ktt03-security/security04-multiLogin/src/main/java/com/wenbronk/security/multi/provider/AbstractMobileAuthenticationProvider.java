package com.wenbronk.security.multi.provider;

import com.wenbronk.security.multi.token.MobilAuthenticationToken;
import lombok.Data;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.util.Assert;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/7 10:32 上午
 * description:
 */
@Data
public abstract class AbstractMobileAuthenticationProvider extends AbstractAuthenticationProvider implements AuthenticationProvider {

    private UserCache userCache = new NullUserCache();
    protected boolean hideUserNotFoundExceptions = true;
    private boolean forcePrincipalAsString = false;
    private UserDetailsChecker preAuthenticationChecks = new AbstractAuthenticationProvider().new DefaultPreAuthenticationChecks();
    private UserDetailsChecker postAuthenticationChecks = new AbstractAuthenticationProvider().new DefaultPostAuthenticationChecks();
    private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.isInstanceOf(MobilAuthenticationToken.class, authentication,
                () -> messages.getMessage(
                        "AbstractUserDetailsAuthenticationProvider.onlySupports",
                        "Only UsernamePasswordAuthenticationToken is supported"));

        // Determine username
        String mobile = (authentication.getPrincipal() == null) ? "NONE_PROVIDED"
                : authentication.getName();

        boolean cacheWasUsed = true;
        UserDetails user = this.userCache.getUserFromCache(mobile);

        if (user == null) {
            cacheWasUsed = false;

            try {
                user = retrieveUser(mobile, (MobilAuthenticationToken) authentication);
            } catch (UsernameNotFoundException notFound) {
//                logger.debug("User '" + username + "' not found");

                if (hideUserNotFoundExceptions) {
                    throw new BadCredentialsException(messages.getMessage(
                            "AbstractUserDetailsAuthenticationProvider.badCredentials",
                            "Bad credentials"));
                } else {
                    throw notFound;
                }
            }

            Assert.notNull(user, "retrieveUser returned null - a violation of the interface contract");
        }

        try {
            preAuthenticationChecks.check(user);
            additionalAuthenticationChecks(user, (MobilAuthenticationToken) authentication);
        } catch (AuthenticationException exception) {
            if (cacheWasUsed) {
                cacheWasUsed = false;
                user = retrieveUser(mobile, (MobilAuthenticationToken) authentication);
                preAuthenticationChecks.check(user);
                additionalAuthenticationChecks(user, (MobilAuthenticationToken) authentication);
            } else {
                throw exception;
            }
        }

        postAuthenticationChecks.check(user);

        if (!cacheWasUsed) {
            this.userCache.putUserInCache(user);
        }

        Object principalToReturn = user;

        if (forcePrincipalAsString) {
            principalToReturn = user.getUsername();
        }

        return createSuccessAuthentication(principalToReturn, authentication, user);
    }

    /**
     * 成功后创建authentication
     */
    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
        MobilAuthenticationToken result = new MobilAuthenticationToken(
                principal, authentication.getCredentials(),
                authoritiesMapper.mapAuthorities(user.getAuthorities()));
        result.setDetails(authentication.getDetails());

        return result;
    }

    protected abstract UserDetails retrieveUser(String username, MobilAuthenticationToken authentication)
            throws AuthenticationException;

    protected abstract void additionalAuthenticationChecks(UserDetails userDetails, MobilAuthenticationToken authentication)
            throws AuthenticationException;


    /**
     * 判断是否支持
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return MobilAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
