package com.wenbronk.security.multi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk
 * @Date 2019/11/5 3:33 下午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security.multi"})
public class MultiLoginMain {
    public static void main(String[] args) {
        SpringApplication.run(MultiLoginMain.class, args);
    }
}
