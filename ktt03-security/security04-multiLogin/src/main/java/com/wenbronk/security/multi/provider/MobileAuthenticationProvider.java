package com.wenbronk.security.multi.provider;

import com.wenbronk.security.multi.service.MultiDetailsService;
import com.wenbronk.security.multi.token.MobilAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/6 3:59 下午
 * description: 理论上继承 AuthenticationProvider
 */
@Component
public class MobileAuthenticationProvider extends AbstractMobileAuthenticationProvider {

    @Autowired
    private MultiDetailsService userDetailsService;

    /**
     * 验证码校验
     * 一般放redis中， 这儿直接返回
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, MobilAuthenticationToken authentication) throws AuthenticationException {
        if (authentication.getCredentials() == null) {
//            logger.debug("Authentication failed: no credentials provided");

            throw new BadCredentialsException(messages.getMessage(
                    "AbstractUserDetailsAuthenticationProvider.badCredentials",
                    "Bad credentials"));
        }
        return;
    }

    /**
     * 获取真实 user对象
     */
    @Override
    protected UserDetails retrieveUser(String mobile, MobilAuthenticationToken authentication) throws AuthenticationException {
        try {
            UserDetails loadedUser = userDetailsService.loadUserByMobile(mobile);
            if (loadedUser == null) {
                throw new InternalAuthenticationServiceException(
                        "UserDetailsService returned null, which is an interface contract violation");
            }
            return loadedUser;
        } catch (UsernameNotFoundException | InternalAuthenticationServiceException ex) {
            // 加密的, 不需要
//            mitigateAgainstTimingAttack(authentication);
            throw ex;
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex);
        }
    }
}
