package com.wenbronk.security08.client.feign.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/25 4:32 下午
 * description: 将所有的feign head 向下传递， 包括authorization认证
 */
@Component
public class OAuthFeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletRequest request = requestAttributes.getRequest();
            // 从header中找到令牌
            Enumeration<String> headerNames = request.getHeaderNames();
            if (headerNames != null) {
                while (headerNames.hasMoreElements()) {
                    String headName = headerNames.nextElement();
                    String value = request.getHeader(headName);
                    requestTemplate.header(headName, value);
                }
            }
        }
    }
}
