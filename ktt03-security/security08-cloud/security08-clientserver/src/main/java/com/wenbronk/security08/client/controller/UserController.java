package com.wenbronk.security08.client.controller;

import com.wenbronk.security08.client.service.FeignUserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/25 3:44 下午
 * description:
 */
@RestController
@RequestMapping(value = {"/user"})
public class UserController {

    @Resource
    public FeignUserService feignUserService;

    @RequestMapping("/getUserExt/{id}")
    public String getUserExt(@PathVariable("id") String id) {
        return feignUserService.getUserExt(id);
    }

    @RequestMapping("/getUser")
    public String test() {
        return "get user from client";
    }

}
