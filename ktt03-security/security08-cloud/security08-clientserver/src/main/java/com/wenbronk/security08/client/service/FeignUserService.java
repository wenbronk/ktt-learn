package com.wenbronk.security08.client.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/23 11:28 上午
 * description:
 */
@FeignClient(name = "resource-server", fallback = HystrixUserService.class)
public interface FeignUserService {

    @RequestMapping(value = "/user/getUserExt/{id}", method = RequestMethod.GET)
    String getUserExt(@PathVariable("id") String id);

}
