package com.wenbronk.security08.client.service;

import org.springframework.stereotype.Component;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/23 1:10 下午
 * description:
 */
@Component
public class HystrixUserService implements FeignUserService {
    @Override
    public String getUserExt(String id) {
        return "get user from feign error";
    }
}
