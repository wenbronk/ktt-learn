package com.wenbronk.security08.client;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/25 2:33 下午
 * description:
 */
@SpringCloudApplication
@EnableFeignClients
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security08.client"})
public class ClientMain {
    public static void main(String[] args) {
        SpringApplication.run(ClientMain.class, args);
    }
}
