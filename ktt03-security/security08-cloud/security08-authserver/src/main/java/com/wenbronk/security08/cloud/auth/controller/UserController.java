package com.wenbronk.security08.cloud.auth.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/23 1:07 下午
 * description:
 */
@RestController
@RequestMapping(value = "/user")
@RefreshScope
public class UserController {

    @Value("${abc.name:abc}")
    private String name;

    @RequestMapping("/getConfig")
    public String getConfig() {
        return "hello configggg";
    }

}
