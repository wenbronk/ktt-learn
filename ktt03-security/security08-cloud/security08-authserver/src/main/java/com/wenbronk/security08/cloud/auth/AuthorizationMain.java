package com.wenbronk.security08.cloud.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/22 2:11 下午
 * description:
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security08.cloud.auth"})
public class AuthorizationMain {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AuthorizationMain.class, args);
        System.out.println(context.getEnvironment().getProperty("abc.name"));
    }
}
