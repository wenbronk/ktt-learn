package com.wenbronk.security08.cloud.auth.config;

import com.wenbronk.security08.cloud.auth.model.UserJwt;
import com.wenbronk.security08.cloud.auth.service.CustomerUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.bootstrap.encrypt.KeyProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.security.KeyPair;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/25 9:52 上午
 * description:
 */
@Configuration
@EnableConfigurationProperties(KeyProperties.class)
public class TokenConfig {

    @Autowired
    private KeyProperties keyProperties;

    @Bean
    public JwtTokenStore jwtTokenStore(JwtAccessTokenConverter jwtAccessTokenConverter) {
        return new JwtTokenStore(jwtAccessTokenConverter);
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(CustomUserTokenConvert customUserTokenConvert) {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();

        KeyProperties.KeyStore keyStore = keyProperties.getKeyStore();
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(keyStore.getLocation(), keyStore.getSecret().toCharArray());
        // 把密码放配置文件会报错。。
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair(keyStore.getAlias(), "a75767626".toCharArray());
        jwtAccessTokenConverter.setKeyPair(keyPair);

        DefaultAccessTokenConverter accessTokenConverter = (DefaultAccessTokenConverter) jwtAccessTokenConverter.getAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(customUserTokenConvert);
        return jwtAccessTokenConverter;
    }

    @Component
    public static class CustomUserTokenConvert extends DefaultUserAuthenticationConverter {

        @Autowired
        private CustomerUserDetailsService customerUserDetailsService;

        @Override
        public Map<String, ?> convertUserAuthentication(Authentication authentication) {
            Object principal = authentication.getPrincipal();
            UserJwt userJwt = null;
            String name = authentication.getName();
            if (principal instanceof UserJwt) {
                userJwt = (UserJwt) principal;
            } else {
                userJwt = (UserJwt) customerUserDetailsService.loadUserByUsername(name);
            }

            LinkedHashMap<String, Object> response = new LinkedHashMap<>();
            response.put("user_name", name);
            response.put("id", userJwt.getId());
            response.put("utype", userJwt.getUtype());
            if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
                response.put("authorities", AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
            }
            return response;
        }
    }

}
