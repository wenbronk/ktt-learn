package com.wenbronk.security08.cloud.auth.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/24 10:28 下午
 * description:
 */
public interface MultiDetailsService extends UserDetailsService {

    UserDetails loadUserByMobile(String mobile);

}
