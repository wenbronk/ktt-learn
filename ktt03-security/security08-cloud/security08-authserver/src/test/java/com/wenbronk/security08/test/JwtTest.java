package com.wenbronk.security08.test;

import org.junit.Test;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/25 2:12 下午
 * description:
 */
public class JwtTest {

    @Test
    public void decode() {
        String accessToke = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbImFwcCIsInJlYWRfdXNlcmluZm8iXSwidXR5cGUiOm51bGwsImF1dGhvcmllcyI6WyJhZG1pbiIsInN0YWZmIiwic3RhZmYg5p2D6ZmQIiwiYWRtaW4g5p2D6ZmQIl0sImlkIjpudWxsLCJleHAiOjE1NzQ3MDU0NTcsImp0aSI6ImRkZDU3YjYwLWJjZDktNGI0Yy1hYzgyLWVkNGE2MGRjYTFmMSIsImNsaWVudF9pZCI6ImNsaWVudCJ9.a8oowC5a3bbKbYYLqno3n9bU8pp_98tbZZs5YOvegrKOMRibL2JHTBA_lza3kD1w6FTkY7chFLmdxunNgAPLkwmrfSncGLNdG-Uh2Ydb54IgzlF-4YlNP8j_C-aLaiV954tNCQO2Ezy4GHi_EcxFgKh_Os_AoZvdC8wWrbSz5iRu59Rxncmrm071Ja0NipQE2XbpN9Y3B-_sANE3-nSMQm-_QXFQdeFfUUEnjZjoNXPWBFtCTFs4xHGI9rWYYLNjmmYbA4fqtwP8YS7otyyIckNFddAIe--u0TfhRbJl2sX-TrwQz-bTMo0ZAyt7y2xRBwBSSW_7DKoPA7kzc0a70w";
        Jwt jwt = JwtHelper.decode(accessToke);
        String claims = jwt.getClaims();
        System.out.println(claims);
    }

}
