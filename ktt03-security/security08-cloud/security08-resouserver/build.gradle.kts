plugins {
    id("org.springframework.boot")
}
springBoot {
    mainClassName = "com.wenbronk.security08.cloud.resource.ResourceServerMain"
}

springBoot {
    buildInfo {
        properties {
            artifact = "${project.name}"
            version = "${project.version}"
            group = "${project.group}"
            name = "testresource"
        }
    }
}
dependencies {
    implementation(project(":ktt03-security:security00-base"))
    implementation("mysql:mysql-connector-java")

    implementation("org.springframework.cloud:spring-cloud-starter")
    implementation("org.springframework.cloud:spring-cloud-starter-alibaba-nacos-discovery")
    implementation("org.springframework.cloud:spring-cloud-starter-alibaba-nacos-config")
//    自带hystrix， 不需要引入
    implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
    implementation("org.springframework.cloud:spring-cloud-starter-netflix-hystrix")

    implementation("org.springframework.cloud:spring-cloud-starter-oauth2")
//      cloud oauth2 包含了
//    implementation("org.springframework.boot:spring-boot-starter-security")
//    implementation("org.springframework.security.oauth.boot:spring-security-oauth2-autoconfigure:2.2.0.RELEASE")
//    implementation("io.jsonwebtoken:jjwt:0.9.0")

}