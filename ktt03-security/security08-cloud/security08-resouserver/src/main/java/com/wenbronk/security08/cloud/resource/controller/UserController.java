package com.wenbronk.security08.cloud.resource.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/23 11:26 上午
 * description:
 */
@RestController
@RequestMapping(value = {"/user"})
public class UserController {

    @RequestMapping("/getUserExt/{id}")
    public String getUserExt(@PathVariable("id") String id) {
        return "get user from resource server: " + id;
    }

    @RequestMapping("/getUser")
    public String getuser() {
        return "hello user";
    }

}
