package com.wenbronk.security08.cloud.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/22 10:44 下午
 * description:
 */
@SpringCloudApplication
@EnableFeignClients
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security08.cloud.resource"})
public class ResourceServerMain {
    public static void main(String[] args) {
        SpringApplication.run(ResourceServerMain.class, args);
    }
}
