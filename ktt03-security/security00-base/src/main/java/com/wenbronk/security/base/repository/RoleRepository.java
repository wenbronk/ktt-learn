package com.wenbronk.security.base.repository;

import com.wenbronk.security.base.model.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:34 下午
 * description:
 */
public interface RoleRepository extends JpaRepository<SysRole, String> {

    SysRole findByRoleName(String roleName);

}
