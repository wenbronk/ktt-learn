package com.wenbronk.security.base.repository;

import com.wenbronk.security.base.model.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:24 下午
 * description:
 */
public interface UserRepository extends JpaRepository<SysUser, String> {

    SysUser findByUsername(String userName);

    SysUser findByPhone(String phone);

}
