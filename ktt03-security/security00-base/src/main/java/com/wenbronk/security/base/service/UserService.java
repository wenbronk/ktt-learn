package com.wenbronk.security.base.service;

import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.repository.RoleRepository;
import com.wenbronk.security.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:25 下午
 * description:
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public void save(SysUser user) {
        userRepository.save(user);
    }

    public void deleteById(String id) {
        userRepository.deleteById(id);
    }

    public SysUser findByUserName(String userName) {
        return userRepository.findByUsername(userName);
    }

    @Transactional(rollbackOn = Exception.class)
    public void assignRole2User(String userId, List<String> roleIds) {
        SysUser user = userRepository.findById(userId).get();
        List<SysRole> allById = roleRepository.findAllById(roleIds);
        user.setRoles(new HashSet<>(allById));
        userRepository.save(user);
    }

    public SysUser findByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }

}
