package com.wenbronk.security.base.utils;

import com.wenbronk.security.base.model.SysUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 描述：
 *
 * @Author shf
 * @Date 2019/4/21 15:24
 * @Version V1.0
 **/
public class UserHolder {
    public static SysUser getUserDetail(){
        SecurityContext ctx = SecurityContextHolder.getContext();
        Authentication auth = ctx.getAuthentication();
        SysUser user = (SysUser) auth.getPrincipal();
        return user;
    }
    public static String getUserId(){
        SecurityContext ctx = SecurityContextHolder.getContext();
        Authentication auth = ctx.getAuthentication();
        SysUser user = (SysUser) auth.getPrincipal();
        return user.getId();
    }
}
