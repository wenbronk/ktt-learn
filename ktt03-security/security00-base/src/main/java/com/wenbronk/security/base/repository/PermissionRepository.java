package com.wenbronk.security.base.repository;

import com.wenbronk.security.base.model.SysPermission;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:34 下午
 * description:
 */
public interface PermissionRepository extends JpaRepository<SysPermission, String> {

    SysPermission findByName(String name);

}
