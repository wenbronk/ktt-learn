package com.wenbronk.security.base.controller;

import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019/10/24 5:52 下午
 * description:
 */
@RestController
@RequestMapping("/user")
public class SysUserController {

    @Autowired
    private UserService userService;

    /**
     * 登陆失败跳转
     */
    @RequestMapping("/failure")
    public String loginFailure() {
        return "login failure";
    }

    /**
     * 登陆成功跳转
     */
    @PostMapping("/success")
    public String loginSuccess() {
        // 登录成功后用户的认证信息 UserDetails会存在 安全上下文寄存器 SecurityContextHolder 中
        SysUser principal = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.getUsername();
        SysUser sysUser = userService.findByUserName(username);
        // 脱敏
        sysUser.setPassword("[PROTECT]");
        return "登录成功";
    }



    @RequestMapping("/logout")
    public String logout(String userName) {
        System.out.println("logout userName: " + userName);
        return userName;
    }

    @RequestMapping("/findAll")
    public String findAll() {
        System.out.println("find All");
        return "find All";
    }

    @PreAuthorize("hasAnyRole('root')")
    @RequestMapping("/addUser")
    public String addUser() {
        System.out.println("add user");
        return "add User";
    }

    @PreAuthorize("hasAnyRole('admin', 'root') or hasAnyAuthority('用户删除')")
    @RequestMapping("/deleteUser")
    public String deleteUser() {
        System.out.println("delete user");
        return "delete User";
    }

    @PreAuthorize("hasAnyAuthority('admin 权限')")
    @RequestMapping("/admin")
    public String admin() {
        System.out.println("admin user");
        return "admin User";
    }

    @RequestMapping("/staff")
    @PreAuthorize("hasAnyAuthority('staff 权限')")
    public String staff() {
        System.out.println("staff user");
        return "staff User";
    }

}
