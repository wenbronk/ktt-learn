package com.wenbronk.security.base.controller;

import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019/10/31 5:16 下午
 * description:
 */
@RestController
@RequestMapping("/sys")
public class AbstractLoginController {

    @Autowired
    private UserService userService;

    /**
     * 登陆失败跳转
     */
    @RequestMapping("/failure")
    public String loginFailure() {
        return "login failure";
    }

    /**
     * 登陆成功跳转
     */
    @PostMapping("/success")
    public String loginSuccess() {
        // 登录成功后用户的认证信息 UserDetails会存在 安全上下文寄存器 SecurityContextHolder 中
        SysUser principal = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.getUsername();
        SysUser sysUser = userService.findByUserName(username);
        // 脱敏
        sysUser.setPassword("[PROTECT]");
        return "登录成功";
    }

}
