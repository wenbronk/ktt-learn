package com.wenbronk.security.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author wenbronk
 * @Date 2019/10/31 10:55 下午
 * description:
 */
@SpringBootApplication
public class SecurityBaseMain {
    public static void main(String[] args) {
        SpringApplication.run(SecurityBaseMain.class, args);
    }
}
