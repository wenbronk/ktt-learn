package com.wenbronk.security.base.service;

import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.base.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author wenbronk
 * @Date 2019/10/24 3:10 下午
 * description:
 */
@Service
public class PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    public void save(SysPermission permission) {
        permissionRepository.save(permission);
    }

    public SysPermission findByName(String name) {
        return permissionRepository.findByName(name);
    }

    public SysPermission findById(String id) {
        return permissionRepository.findById(id).get();
    }

}
