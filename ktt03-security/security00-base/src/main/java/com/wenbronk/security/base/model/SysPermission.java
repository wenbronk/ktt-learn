package com.wenbronk.security.base.model;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:28 下午
 * description:
 */
@Entity
@Data
@Table
@DynamicInsert(true)
@DynamicUpdate(true)
public class SysPermission {

    @Id
    private String id;
    /**
     * 权限名称
     */
    private String name;

    /**
     * 链接
     */
    private String apiUrl;

}
