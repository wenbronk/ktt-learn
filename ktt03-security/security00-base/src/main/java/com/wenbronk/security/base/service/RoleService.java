package com.wenbronk.security.base.service;

import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:34 下午
 * description:
 */
@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public void save(SysRole role) {
        roleRepository.save(role);
    }

    public SysRole findByRoleName(String roleName) {
        return roleRepository.findByRoleName(roleName);
    }

}
