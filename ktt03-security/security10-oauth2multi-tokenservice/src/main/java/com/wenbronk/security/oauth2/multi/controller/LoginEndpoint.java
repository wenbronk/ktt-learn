package com.wenbronk.security.oauth2.multi.controller;

import com.google.common.collect.Maps;
import com.wenbronk.security.oauth2.multi.mobile.MobileAuthenticationToken;
import lombok.Data;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.token.TokenService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidRequestException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.endpoint.AbstractEndpoint;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestValidator;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.Collections;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/21 1:26 下午
 * description:
 */
@RestController
@Data
public class LoginEndpoint extends AbstractEndpoint {

    private String credentialsCharset = "UTF-8";
    private String grantType = "custome";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private OAuth2RequestValidator oAuth2RequestValidator = new DefaultOAuth2RequestValidator();

    @Autowired
    private AuthorizationServerTokenServices jwtTokenServices;

    @RequestMapping("/sys/username")
    public ResponseEntity<OAuth2AccessToken> login(HttpServletRequest request, String username, String password, String grantType) throws IOException {
        Authentication client = getClient(request);
        ClientDetails clientDetails = checkClient(client);

        AbstractAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        OAuth2AccessToken oAuth2AccessToken = this.applyToken(clientDetails, authenticationToken);
        return getResponse(oAuth2AccessToken);
    }

    @RequestMapping("/sys/mobile")
    public ResponseEntity<OAuth2AccessToken> login2(HttpServletRequest request, String mobile, String certify) throws IOException {
        Authentication client = getClient(request);
        ClientDetails clientDetails = checkClient(client);

        AbstractAuthenticationToken authenticationToken = new MobileAuthenticationToken(mobile, certify);
        OAuth2AccessToken oAuth2AccessToken = this.applyToken(clientDetails, authenticationToken);
        return getResponse(oAuth2AccessToken);
    }

    /**
     * 获取token
     */
    public OAuth2AccessToken applyToken(ClientDetails clientDetails, Authentication authentication) {
        Authentication authenticate = authenticationManager.authenticate(authentication);
        SecurityContextHolder.getContext().setAuthentication(authenticate);

        TokenRequest tokenRequest = new TokenRequest(Collections.EMPTY_MAP, clientDetails.getClientId(), clientDetails.getScope(), grantType);
        OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);

        OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authenticate);
        OAuth2AccessToken accessToken = jwtTokenServices.createAccessToken(oAuth2Authentication);
        oAuth2Authentication.setAuthenticated(true);
        return accessToken;
    }

    /**
     * 检查clientId 和 clientSecurity
     */
    public ClientDetails checkClient(Authentication authentication) {
        ClientDetails clientDetails = super.getClientDetailsService().loadClientByClientId(authentication.getPrincipal().toString());
        TokenRequest tokenRequest = getOAuth2RequestFactory().createTokenRequest(Maps.newHashMap(), clientDetails);

        if (clientDetails != null) {
            oAuth2RequestValidator.validateScope(tokenRequest, clientDetails);
        }

        if (!passwordEncoder.matches(authentication.getCredentials().toString(), clientDetails.getClientSecret())) {
            logger.debug("Authentication failed: password does not match stored value");
            throw new BadCredentialsException(
                    "AbstractUserDetailsAuthenticationProvider.badCredentials" +
                    "Bad credentials");
        }
        return clientDetails;
    }

    /**
     * 获取client相关
     */
    private Authentication getClient(HttpServletRequest request) throws IOException {
        String header = request.getHeader("Authorization");
        if (header == null || !header.toLowerCase().startsWith("basic ")) {
            throw new InsufficientAuthenticationException("There is no client authentication. Try adding an appropriate authentication filter.");
        }
        String[] tokens = extractAndDecodeHeader(header, request);
        assert tokens.length == 2;
        String clientId = tokens[0];
        Authentication authRequest = new UsernamePasswordAuthenticationToken(clientId, tokens[1]);
        return authRequest;
    }
    private String[] extractAndDecodeHeader(String header, HttpServletRequest request)
            throws IOException {
        byte[] base64Token = header.substring(6).getBytes("UTF-8");
        byte[] decoded;
        try {
            decoded = Base64.getDecoder().decode(base64Token);
        } catch (IllegalArgumentException e) {
            throw new BadCredentialsException("Failed to decode basic authentication token");
        }
        String token = new String(decoded, getCredentialsCharset(request));
        int delim = token.indexOf(":");
        if (delim == -1) {
            throw new BadCredentialsException("Invalid basic authentication token");
        }
        return new String[]{token.substring(0, delim), token.substring(delim + 1)};
    }
    protected String getCredentialsCharset(HttpServletRequest httpRequest) {
        return this.credentialsCharset;
    }

    private ResponseEntity<OAuth2AccessToken> getResponse(OAuth2AccessToken accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Cache-Control", "no-store");
        headers.set("Pragma", "no-cache");
        headers.set("Content-Type", "application/json;charset=UTF-8");
        return new ResponseEntity<OAuth2AccessToken>(accessToken, headers, HttpStatus.OK);
    }

}
