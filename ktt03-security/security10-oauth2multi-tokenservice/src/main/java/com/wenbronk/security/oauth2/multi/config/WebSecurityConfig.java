package com.wenbronk.security.oauth2.multi.config;

import com.wenbronk.security.oauth2.multi.handler.CustomAccessDeniedHandler;
import com.wenbronk.security.oauth2.multi.handler.CustomAuthenticationEntryPoint;
import com.wenbronk.security.oauth2.multi.handler.LoginoutHandler;
import com.wenbronk.security.oauth2.multi.mobile.MobileAuthenticationPorvider;
import com.wenbronk.security.oauth2.multi.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/21 9:36 上午
 * description:
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    public MobileAuthenticationPorvider mobileAuthenticationPorvider;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    /**
     * 去除角色前缀 ROLE_ 的
     */
    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults(""); // Remove the ROLE_ prefix
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService)
                .passwordEncoder(bCryptPasswordEncoder());
        auth.authenticationProvider(mobileAuthenticationPorvider);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/sys/login");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();  // 关闭csrf支持
        http.cors();            // 启用跨域
        http.requestMatchers()
                .antMatchers("/sys/username", "/sys/mobile");
        http.authorizeRequests()
                .antMatchers("/sys/username", "/sys/mobile").permitAll()
                .anyRequest().authenticated();
        http.exceptionHandling()
                .authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                .logout()
                .logoutSuccessHandler(new LoginoutHandler());
    }


}
