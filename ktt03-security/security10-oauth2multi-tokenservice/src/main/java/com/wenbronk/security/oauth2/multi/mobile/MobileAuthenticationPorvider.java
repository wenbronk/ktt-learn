package com.wenbronk.security.oauth2.multi.mobile;

import com.wenbronk.security.oauth2.multi.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/21 10:01 上午
 * description:
 */
@Component
//@ConditionalOnBean(DaoAuthenticationProvider.class)
public class MobileAuthenticationPorvider extends AbstractMobileAuthenticationProvider {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    /**
     * 查询用户
     */
    @Override
    protected UserDetails retrieveUser(String mobile, MobileAuthenticationToken authentication) throws AuthenticationException {
        try {
            UserDetails loadedUser = customUserDetailsService.loadUserByMobile(mobile);
            if (loadedUser == null) {
                throw new InternalAuthenticationServiceException(
                        "UserDetailsService returned null, which is an interface contract violation");
            }
            return loadedUser;
        } catch (UsernameNotFoundException | InternalAuthenticationServiceException ex) {
            // 加密的, 不需要
//            mitigateAgainstTimingAttack(authentication);
            throw ex;
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex);
        }
    }

    /**
     * 校验验证码， 一般放在redis中， 这儿直接返回
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, MobileAuthenticationToken authentication) throws AuthenticationException {
        if (authentication.getCredentials() == null) {
//            logger.debug("Authentication failed: no credentials provided");

            throw new BadCredentialsException(messages.getMessage(
                    "AbstractUserDetailsAuthenticationProvider.badCredentials",
                    "Bad credentials"));
        }

        if (! authentication.getCredentials().toString().equalsIgnoreCase("1234")) {
            throw new BadCredentialsException(messages.getMessage(
                    "AbstractUserDetailsAuthenticationProvider.badCredentials",
                    "Bad yanzhengma"));
        }

        return;
    }
}
