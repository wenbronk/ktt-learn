package com.wenbronk.security.oauth2.multi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/21 9:34 上午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security.oauth2.multi"})
public class OAuthMultiTokenMain {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(OAuthMultiTokenMain.class, args);
        System.out.println(context);
    }
}
