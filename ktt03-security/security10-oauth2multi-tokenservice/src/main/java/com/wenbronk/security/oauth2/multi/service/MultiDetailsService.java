package com.wenbronk.security.oauth2.multi.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/21 9:38 上午
 * description:
 */
public interface MultiDetailsService extends UserDetailsService {

    UserDetails loadUserByMobile(String mobile);

}
