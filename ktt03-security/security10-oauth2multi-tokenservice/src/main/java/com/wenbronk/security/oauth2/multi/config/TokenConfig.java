package com.wenbronk.security.oauth2.multi.config;

import ch.qos.logback.core.net.ssl.KeyStoreFactoryBean;
import com.wenbronk.security.oauth2.multi.model.UserJwt;
import com.wenbronk.security.oauth2.multi.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/21 10:39 上午
 * description:
 */
@Configuration
public class TokenConfig {

    @Bean
    public JwtTokenStore jwtTokenStore(JwtAccessTokenConverter jwtAccessTokenConverter) {
        return new JwtTokenStore(jwtAccessTokenConverter);
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(CustomUserTokenConvert tokenConvert) {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("wenbronk.jks"), "a75767626store".toCharArray());
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair("wenbronk", "a75767626".toCharArray());
        jwtAccessTokenConverter.setKeyPair(keyPair);

        DefaultAccessTokenConverter accessTokenConverter = (DefaultAccessTokenConverter) jwtAccessTokenConverter.getAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(tokenConvert);
        return jwtAccessTokenConverter;
    }

    /**
     * token convert
     */
    @Component
    public class CustomUserTokenConvert extends DefaultUserAuthenticationConverter {

        @Autowired
        private CustomUserDetailsService userDetailsService;

        @Override
        public Map<String, ?> convertUserAuthentication(Authentication authentication) {

            Object principal = authentication.getPrincipal();

            UserJwt userJwt = null;
            String name = authentication.getName();

            if (principal instanceof UserJwt) {
                userJwt = (UserJwt) principal;
            } else {
                // refresh token 默认不调用， 需要手动调用
                userJwt = (UserJwt) userDetailsService.loadUserByUsername(name);
            }

            LinkedHashMap<String, Object> response = new LinkedHashMap<>();
            response.put("user_name", name);
            response.put("id", userJwt.getId());
            response.put("utype", userJwt.getUtype());
            if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
                response.put("authories", AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
            }
            return response;
        }
    }
 }
