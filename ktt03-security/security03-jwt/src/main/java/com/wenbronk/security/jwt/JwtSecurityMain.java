package com.wenbronk.security.jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk
 * @Date 2019/11/1 1:38 下午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security.jwt"})
public class JwtSecurityMain {
    public static void main(String[] args) {
        SpringApplication.run(JwtSecurityMain.class, args);
    }
}
