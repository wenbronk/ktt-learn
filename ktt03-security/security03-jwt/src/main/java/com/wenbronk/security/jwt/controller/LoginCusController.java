package com.wenbronk.security.jwt.controller;

import com.wenbronk.security.base.utils.Response;
import com.wenbronk.security.jwt.service.LoginCusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019/11/1 1:55 下午
 * description:
 */
@RestController
@RequestMapping("/sys")
public class LoginCusController {

    @Autowired
    private LoginCusService loginCusService;

    /**
     * 自定义登陆接口
     */
    @RequestMapping("/login")
    public Response login(String username, String password) {
        Response response = new Response().buildSuccessResponse();
        String login = loginCusService.login(username, password);
        response.setData(login);
        return response;
    }

}
