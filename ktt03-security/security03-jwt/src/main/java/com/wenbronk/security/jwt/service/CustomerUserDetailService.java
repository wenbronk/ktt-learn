package com.wenbronk.security.jwt.service;

import com.google.common.collect.Lists;
import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.jwt.model.SysUserDetails;
import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Set;

/**
 * @Author wenbronk
 * @Date 2019/11/1 1:44 下午
 * description: 内置登陆类
 */
@Service
public class CustomerUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = userRepository.findByUsername(username);
        if(sysUser == null) throw new RuntimeException("用户名或密码没找到");

        Set<SysRole> roles = sysUser.getRoles();
        ArrayList<GrantedAuthority> grantedAuthorities = Lists.newArrayList();

        for (SysRole role : roles) {
            for (SysPermission permission : role.getPermissions()) {
                SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(permission.getApiUrl());
                grantedAuthorities.add(simpleGrantedAuthority);
            }
        }
        return new SysUserDetails(sysUser, grantedAuthorities);
    }
}
