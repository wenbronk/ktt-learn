package com.wenbronk.security.role.utils;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author wenbronk
 * @Date 2019/11/1 11:37 上午
 * description:
 */
public class RequestUtil {
    public static boolean matchers(String url, HttpServletRequest request) {
        AntPathRequestMatcher matcher = new AntPathRequestMatcher(url);
        if (matcher.matches(request)) {
            return true;
        }
        return false;
    }
}
