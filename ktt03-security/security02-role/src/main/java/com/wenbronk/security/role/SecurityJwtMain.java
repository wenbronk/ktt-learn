package com.wenbronk.security.role;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk
 * @Date 2019/11/1 9:14 上午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security.role"})
public class SecurityJwtMain {
    public static void main(String[] args) {
        SpringApplication.run(SecurityJwtMain.class, args);
    }
}
