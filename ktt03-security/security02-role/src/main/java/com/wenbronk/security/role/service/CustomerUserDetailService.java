package com.wenbronk.security.role.service;

import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.service.UserService;
import com.wenbronk.security.role.model.SysUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @Author wenbronk
 * @Date 2019/11/1 9:26 上午
 * description:
 */
@Service
public class CustomerUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = userService.findByUserName(username);
        Set<SysRole> roles = sysUser.getRoles();

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (SysRole role : roles) {
            for (SysPermission permission : role.getPermissions()) {
                GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(permission.getApiUrl());
                grantedAuthorities.add(grantedAuthority);
            }
        }
        return new SysUserDetails(sysUser, grantedAuthorities);
    }
}
