package com.wenbronk.security.role.model;

import com.wenbronk.security.base.model.SysUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;


/**
 * @Author wenbronk
 * @Date 2019/10/31 7:35 下午
 * description:
 */
@Data
public class SysUserDetails extends SysUser implements UserDetails {

    private Collection<? extends GrantedAuthority> authorities;

    public SysUserDetails(SysUser sysUser, Collection<? extends GrantedAuthority> authorities) {
        super(sysUser.getId(), sysUser.getPhone(), sysUser.getUsername(), sysUser.getPassword(), sysUser.getRoles());
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
