package com.wenbronk.security.role.manager;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * @Author wenbronk
 * @Date 2019/11/1 1:18 下午
 * description:
 */
@Component
public class CustomAccessDecisionManager implements AccessDecisionManager {
    /**
     * 判定是否拥有权限的决策方法
     * @param authentication CustomUserDetailsService类loadUserByUsername()方法中返回值
     * @param object 包含客户端发起的请求的request信息。
     * @param configAttributes CustomFilterInvocationSecurityMetadataSource类的getAttribute()方法返回值
     * @throws AccessDeniedException
     * @throws InsufficientAuthenticationException
     */
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
        String url;
        for (GrantedAuthority ga : authentication.getAuthorities()) {
            url = ga.getAuthority();
            if(url.equals(request.getRequestURI())){
                return;
            }
        }
        throw new AccessDeniedException("没有权限访问");
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
