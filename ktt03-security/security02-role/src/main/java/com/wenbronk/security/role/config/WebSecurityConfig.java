package com.wenbronk.security.role.config;

import com.wenbronk.security.role.handler.FailedHandler;
import com.wenbronk.security.role.handler.LoginoutHandler;
import com.wenbronk.security.role.handler.SuccessHandler;
import com.wenbronk.security.role.service.CustomerUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @Author wenbronk
 * @Date 2019/11/1 9:54 上午
 * description:
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomerUserDetailService customerUserDetailService;

//    @Autowired
//    private CustomFilterSecurityInterceptor customFilterSecurityInterceptor;

    @Bean
    public PasswordEncoder bcryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //将自定义的CustomAuthenticationProvider装配到AuthenticationManagerBuilder
//        auth.authenticationProvider(customAuthenticationProvider);
        //将自定的CustomUserDetailsService装配到AuthenticationManagerBuilder
        auth.userDetailsService(customerUserDetailService)
                .passwordEncoder(bcryptPasswordEncoder());

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService())
                .passwordEncoder(bcryptPasswordEncoder());
        auth.eraseCredentials(false);
    }

    /**
     * security检验忽略的请求，比如静态资源不需要登录的可在本处配置
     * @param web
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
        //        platform.ignoring().antMatchers("/login");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/permit", "/home").permitAll()
                .anyRequest().authenticated()   // 需登陆
                .and()
                .formLogin()
                .loginPage("/sys/login")
                .loginProcessingUrl("/sys/process")
                .successHandler(successHandler())
                .failureHandler(failedHandler())
                .and()
                .logout()
                .permitAll()
                .logoutSuccessHandler(logoutHandler());
        //.addFilterBefore(customFilterSecurityInterceptor, FilterSecurityInterceptor.class);
    }

    @Bean
    public AuthenticationSuccessHandler successHandler() {
        return new SuccessHandler();
    }
    @Bean
    public AuthenticationFailureHandler failedHandler() {
        return new FailedHandler();
    }
    @Bean
    public LogoutSuccessHandler logoutHandler() {
        return new LoginoutHandler();
    }

}
