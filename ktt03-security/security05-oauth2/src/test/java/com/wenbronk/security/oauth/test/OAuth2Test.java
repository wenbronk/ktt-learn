package com.wenbronk.security.oauth.test;

import com.wenbronk.security.oauth.OauthMain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/11 3:19 下午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {OauthMain.class})
public class OAuth2Test {

    @Autowired
    private RestTemplate restTemplate;

    private String host = "http://localhost:9001";
    private String clientID = "client";
    private String clientSecret = "123";

    /**
     * 验证码模式模式， 需要先从浏览器获取令牌
     */
    @Test
    public void testCode() {

        String uri = "/oauth/token";

        String httpBasic = getHttpBasic(clientID, clientSecret);

        // body
        HashMap<String, String> body = new HashMap<>();
        body.put("", "");

//        restTemplate.postForObject();
    }

    private String getHttpBasic(String clientID, String clientSecret) {
        String auth = clientID + ":" + clientSecret;
        byte[] encode = Base64Utils.encode(auth.getBytes());
        return "Basic " + new String(encode);
    }

}
