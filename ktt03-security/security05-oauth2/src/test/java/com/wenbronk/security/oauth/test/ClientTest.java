package com.wenbronk.security.oauth.test;

import com.wenbronk.security.oauth.OauthMain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/8 4:22 下午
 * description:
 */
//@SpringBootTest(classes = {OauthMain.class})
//@RunWith(SpringRunner.class)
public class ClientTest {

    @Test
    public void test() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123");
        System.out.println(encode);
    }

}
