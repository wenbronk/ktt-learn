package com.wenbronk.security.oauth.config;

import com.wenbronk.security.oauth.service.CustomeUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/7 3:58 下午
 * description:
 */
@Configuration
@EnableWebSecurity(debug = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationSuccessHandler successHandler;

    @Autowired
    private LogoutSuccessHandler logoutHandler;

    @Autowired
    private AuthenticationFailureHandler logfaildHandler;

    @Autowired
    private CustomeUserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/oauth/**");
        super.configure(web);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()    //开启跨域
                .csrf().disable() //禁用csrf
                .headers().frameOptions().disable(); //禁用frame options
        http.httpBasic();
        http.authorizeRequests()
                .antMatchers("/oauth/**", "/oauth2/**", "/img/*").permitAll()    // 放行
                .antMatchers("/order/**").hasAnyRole("USER", "ADMIN")    // 角色
                .anyRequest().authenticated();   // 登陆
        http.formLogin()
                .usernameParameter("username") // default is username
                .passwordParameter("password") // default is password
                .failureHandler(logfaildHandler)
//                .authenticationDetailsSource(authenticationDetailsSource) // 自定义的登陆验证
                .and()
                .logout()
                .permitAll()
                // 登出前调用， 否则信息可能不存在， 可用于日志
//                .addLogoutHandler(logoutHandler())
                .logoutSuccessHandler(logoutHandler);
    }
}
