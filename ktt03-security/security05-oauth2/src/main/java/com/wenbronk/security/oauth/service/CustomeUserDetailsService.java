package com.wenbronk.security.oauth.service;

import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/8 11:06 上午
 * description:
 */
@Service
public class CustomeUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = userRepository.findByUsername(username);
        List<SimpleGrantedAuthority> authorities = sysUser.getRoles().stream().flatMap(role ->
                role.getPermissions().stream().map(SysPermission::getName)
                        .map(SimpleGrantedAuthority::new)
        ).collect(Collectors.toList());

        authorities.addAll(
                sysUser.getRoles().stream().map(SysRole::getRoleName)
                        .map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

        UserDetails userDetails = User.withUsername(sysUser.getUsername())
                .password(sysUser.getPassword())
//                .passwordEncoder(passwordEncoder::encode)
                .authorities(authorities)
                .build();
        return userDetails;
    }
}
