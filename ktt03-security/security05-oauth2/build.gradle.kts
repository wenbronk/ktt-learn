dependencies {
    implementation(project(":ktt03-security:security00-base"))
    implementation("mysql:mysql-connector-java")

    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.security.oauth.boot:spring-security-oauth2-autoconfigure:2.1.0.RELEASE")
    implementation("org.springframework.security:spring-security-jwt:1.0.11.RELEASE")
    implementation("io.jsonwebtoken:jjwt:0.9.0")
}