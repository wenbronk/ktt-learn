package com.wenbronk.security.oauthmulti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/18 1:31 下午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security.oauthmulti"})
public class OAuhtMultiMain {
    public static void main(String[] args) {
        SpringApplication.run(OAuhtMultiMain.class, args);
    }
}
