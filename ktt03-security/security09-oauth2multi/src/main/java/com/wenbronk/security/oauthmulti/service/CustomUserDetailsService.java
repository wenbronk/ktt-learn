package com.wenbronk.security.oauthmulti.service;

import com.google.common.collect.Lists;
import com.wenbronk.security.base.model.SysPermission;
import com.wenbronk.security.base.model.SysRole;
import com.wenbronk.security.base.model.SysUser;
import com.wenbronk.security.base.service.UserService;
import com.wenbronk.security.oauthmulti.model.UserJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/18 1:52 下午
 * description:
 */
@Service
public class CustomUserDetailsService implements MultiDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByMobile(String mobile) {
        SysUser sysUser = userService.findByPhone(mobile);
        Collection<GrantedAuthority> authorities = getAuthorities(sysUser);
        return new User(sysUser.getUsername(), sysUser.getPhone(), authorities);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        SysUser sysUser = userService.findByUserName(username);
        Collection<GrantedAuthority> authorities = getAuthorities(sysUser);

        UserDetails userDetails = User.withUsername(sysUser.getUsername())
                .password(sysUser.getPassword())
                .authorities(authorities)
                .build();

        UserJwt userJwt = new UserJwt(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
        userJwt.setCompanyId("asgdsafsafsadfsad");
        return userJwt;
    }

    private Collection<GrantedAuthority> getAuthorities(SysUser sysUser) {
        if(sysUser == null) throw new RuntimeException("用户没找到");
        Set<SysRole> roles = sysUser.getRoles();
        ArrayList<GrantedAuthority> grantedAuthorities = Lists.newArrayList();

        // 把role name 和 permission name 放入认证中
        for (SysRole role : roles) {

            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRoleName()));

            for (SysPermission permission : role.getPermissions()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(permission.getName()));
            }
        }
        return grantedAuthorities;
    }
}
