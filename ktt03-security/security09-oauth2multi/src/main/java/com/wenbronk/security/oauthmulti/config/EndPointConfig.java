package com.wenbronk.security.oauthmulti.config;

import com.wenbronk.security.oauthmulti.controller.LoginEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration;
import org.springframework.security.oauth2.provider.ClientDetailsService;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/18 4:52 下午
 * description:
 */
@Configuration
@Import(AuthorizationServerEndpointsConfiguration.class)
public class EndPointConfig {

    @Autowired
    private AuthorizationServerEndpointsConfiguration configuration;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Bean
    public LoginEndpoint loginEndpoint() throws Exception {
        LoginEndpoint tokenEndpoint = new LoginEndpoint();
        tokenEndpoint.setClientDetailsService(clientDetailsService);
        tokenEndpoint.setProviderExceptionHandler(configuration.getEndpointsConfigurer().getExceptionTranslator());
        tokenEndpoint.setTokenGranter(configuration.getEndpointsConfigurer().getTokenGranter());
        tokenEndpoint.setOAuth2RequestFactory(configuration.getEndpointsConfigurer().getOAuth2RequestFactory());
        tokenEndpoint.setOAuth2RequestValidator(configuration.getEndpointsConfigurer().getOAuth2RequestValidator());
//        tokenEndpoint.setAllowedRequestMethods(configuration.getEndpointsConfigurer().getAllowedTokenEndpointRequestMethods());
        return tokenEndpoint;
    }

}
