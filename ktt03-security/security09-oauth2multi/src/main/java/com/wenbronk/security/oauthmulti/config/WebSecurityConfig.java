package com.wenbronk.security.oauthmulti.config;

import com.wenbronk.security.oauthmulti.handler.CustomAccessDeniedHandler;
import com.wenbronk.security.oauthmulti.handler.CustomAuthenticationEntryPoint;
import com.wenbronk.security.oauthmulti.handler.LoginoutHandler;
import com.wenbronk.security.oauthmulti.mobile.MobileAuthenticationProvider;
import com.wenbronk.security.oauthmulti.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/18 1:43 下午
 * description:
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true) // 控制权限注解
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private MobileAuthenticationProvider mobileAuthenticationProvider;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 去除角色前缀 ROLE_ 的
     * @return
     */
    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults(""); // Remove the ROLE_ prefix
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder());
        auth.authenticationProvider(mobileAuthenticationProvider);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/oauth/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();  // 关闭csrf支持
        http.cors();            // 启用跨域
        http.requestMatchers()
                .antMatchers("/sys/username");
        http.authorizeRequests()
                .antMatchers("/sys/username").permitAll()
                .anyRequest().authenticated();
        http.exceptionHandling()
                .authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                .logout()
                .logoutSuccessHandler(new LoginoutHandler());
    }
}
