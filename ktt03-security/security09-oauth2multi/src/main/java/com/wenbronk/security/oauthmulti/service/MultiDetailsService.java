package com.wenbronk.security.oauthmulti.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/6 4:00 下午
 * description:
 */
public interface MultiDetailsService extends UserDetailsService {

    UserDetails loadUserByMobile(String mobile);

}
