package com.wenbronk.security.oauthjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk <wenbronk@163.com>
 * @Date 2019/11/11 5:06 下午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.security.base", "com.wenbronk.security.oauthjdbc"})
public class JdbcOauthMain {
    public static void main(String[] args) {
        SpringApplication.run(JdbcOauthMain.class, args);
    }
}
