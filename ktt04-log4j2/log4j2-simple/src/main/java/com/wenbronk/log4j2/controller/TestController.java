package com.wenbronk.log4j2.controller;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019/11/4 5:22 下午
 * description:
 */
@RestController
@Data
@Slf4j
public class TestController {

    @RequestMapping("/test")
    public String testLog4J2() {
        log.info("helloeoel, {}", "asdfsdfsadfs");
        return "log success";
    }

}
