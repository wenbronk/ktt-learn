package com.wenbronk.log4j2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author wenbronk
 * @Date 2019/11/4 5:15 下午
 * description:
 */
@SpringBootApplication
public class Log4j2SimpleMain {
    public static void main(String[] args) {
        SpringApplication.run(Log4j2SimpleMain.class, args);
    }
}
