package com.wenbronk.shiro.simple.shiro.realm;

import com.wenbronk.shiro.base.model.User;
import com.wenbronk.shiro.base.service.UserService;
import com.wenbronk.shiro.simple.shiro.token.PhoneIdentiCodeToken;
import org.apache.shiro.authc.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author wenbronk
 * @Date 2019/10/28 9:23 上午
 * description:
 */
public class PhoneIDRealm extends AbstractRealm {

    @Autowired
    private UserService userService;

    @Override
    public void setName(String name) {
        super.setName("customRealm");
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        PhoneIdentiCodeToken phoneIdentiCodeToken = (PhoneIdentiCodeToken) token;
        String phone = phoneIdentiCodeToken.getPhone();
//        String identify = new String(phoneIdentiCodeToken.getIdentify());

        User user = userService.findByPhone(phone);

        return new SimpleAuthenticationInfo(user, user.getPhone(),this.getName());

//        User user = userService.findByUserName(username);
//
//        //4.用户存在并且密码匹配存储用户数据
//        if(user != null && user.getPassword().equals(passwd)) {
//            return new SimpleAuthenticationInfo(user,user.getPassword(),this.getName());
//        }else {
//            //返回null会抛出异常，表明用户不存在或密码不匹配
//            return null;
//        }
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return token != null && PhoneIdentiCodeToken.class.isAssignableFrom(token.getClass());
    }

    /**
     * 断言验证码是否一致
     * @param token
     * @param info
     * @throws AuthenticationException
     */
    @Override
    protected void assertCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) throws AuthenticationException {
        try {
//            super.assertCredentialsMatch(token, info);
            System.out.println("验证码是否一致");

        } catch (IncorrectCredentialsException e) {
            String msg = "Submitted credentials for token [" + token + "] did not match the expected credentials.";
            throw new IncorrectCredentialsException(msg);
        }

    }

}
