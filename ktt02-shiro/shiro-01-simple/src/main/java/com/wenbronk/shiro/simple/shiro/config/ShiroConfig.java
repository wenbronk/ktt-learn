package com.wenbronk.shiro.simple.shiro.config;

import com.google.common.collect.Lists;
import com.wenbronk.shiro.simple.shiro.realm.PhoneIDRealm;
import com.wenbronk.shiro.simple.shiro.realm.UserNameRealm;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author wenbronk
 * @Date 2019/10/24 3:14 下午
 * description:
 */
@Configuration
public class ShiroConfig {

    @Bean
    public UserNameRealm getReal() {
        return new UserNameRealm();
    }

    @Bean
    public PhoneIDRealm getPhoneRealm() {
        return new PhoneIDRealm();
    }

    @Bean
    public SecurityManager securityManager(UserNameRealm userNameRealm, PhoneIDRealm phoneIDRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        List<Realm> realms = Lists.newArrayList(userNameRealm, phoneIDRealm);
        securityManager.setRealms(realms);
        return securityManager;
    }

    @Bean
    public ShiroFilterFactoryBean shirFilter(SecurityManager securityManager) {
        //1.创建shiro过滤器工厂
        ShiroFilterFactoryBean filterFactory = new ShiroFilterFactoryBean();
        //2.设置安全管理器
        filterFactory.setSecurityManager(securityManager);
        //3.通用配置（配置登录页面，登录成功页面，验证未成功页面）
        filterFactory.setLoginUrl("/autherror?code=1"); //设置登录页面
        filterFactory.setUnauthorizedUrl("/autherror?code=2"); //授权失败跳转页面
        //4.配置过滤器集合
        /**
         * key ：访问连接
         * 支持通配符的形式
         * value：过滤器类型
         * shiro常用过滤器
         * anno ：匿名访问（表明此链接所有人可以访问）
         * authc ：认证后访问（表明此链接需登录认证成功之后可以访问）
         */
        Map<String, String> filterMap = new LinkedHashMap<String, String>();
        // 配置不会被拦截的链接 顺序判断
        filterMap.put("/user/login", "anon");
        filterMap.put("/user/mobileLogin", "anon");
        filterMap.put("/user/**", "authc");

        //配置请求连接过滤器配置
        //匿名访问（所有人员可以使用）
//        filterMap.put("/user/home", "anon");
////具有指定权限访问
//        filterMap.put("/user/find", "perms[user-find]");
////认证之后访问（登录之后可以访问）
//        filterMap.put("/user/**", "authc");
////具有指定角色可以访问
//        filterMap.put("/user/**", "roles[系统管理员]");

        //5.设置过滤器
        filterFactory.setFilterChainDefinitionMap(filterMap);
        return filterFactory;
    }

    //配置shiro注解支持
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

}
