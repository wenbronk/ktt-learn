package com.wenbronk.shiro.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk
 * @Date 2019/10/31 10:29 下午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.shiro.base", "com.wenbronk.shiro.simple"})
public class ShiroSimpleMain {
    public static void main(String[] args) {
        SpringApplication.run(ShiroSimpleMain.class, args);
    }
}
