dependencies {
    implementation(project(":ktt02-shiro:shiro-00-base"))

    implementation("mysql:mysql-connector-java")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
}
