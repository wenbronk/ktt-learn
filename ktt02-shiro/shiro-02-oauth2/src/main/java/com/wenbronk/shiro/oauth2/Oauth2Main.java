package com.wenbronk.shiro.oauth2;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:01 下午
 * description:
 */
@SpringBootApplication
@ComponentScan("com.wenbronk.shiro.base")
public class Oauth2Main {
    public static void main(String[] args) {
        SpringApplication.run(Oauth2Main.class, args);
    }
}
