package com.wenbronk.shiro.oauth2.oauth.filter;

import com.wenbronk.shiro.oauth2.oauth.token.OAuht2Token;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author wenbronk
 * @Date 2019/10/28 4:15 下午
 * description:
 */
public class OAuth2AuthenticationFilter extends AuthenticatingFilter {
    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String code = httpRequest.getParameter(OAuth.OAUTH_CODE);
//        log.info("OAuth2AuthenticationFilter中拿到的auth_code = " + code);
        return new OAuht2Token(code);
    }

    // 访问拒绝进入
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        System.out.println("hello");
        return true;
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return false;
    }
}
