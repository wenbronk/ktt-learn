package com.wenbronk.shiro.oauth2.oauth.token;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @Author wenbronk
 * @Date 2019/10/28 4:51 下午
 * description:
 */
@Data
public class OAuht2Token implements AuthenticationToken {

    private String authCode;
    private String principal;

    public OAuht2Token(String authCode) {
        this.authCode = authCode;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public Object getCredentials() {
        return authCode;
    }
}
