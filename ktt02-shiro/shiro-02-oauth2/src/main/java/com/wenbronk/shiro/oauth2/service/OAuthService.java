package com.wenbronk.shiro.oauth2.service;

import com.wenbronk.shiro.oauth2.repository.ClientRepository;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author wenbronk
 * @Date 2019/10/28 11:25 上午
 * description:
 */
@Service
public class OAuthService {

    @Autowired
    private ClientRepository clientRepository;

    private Cache cache;

    @Autowired
    public OAuthService(CacheManager cacheManager) {
        this.cache = cacheManager.getCache("accessTokenCache");
    }

    // 检查客户端id是否存在
    public boolean checkClientId(String clientId, String security) {
        return clientRepository.findByClientId(clientId).isPresent();
    }

    public void addAuthCode(String authCode, String username) {
        cache.put(authCode, username);
    }
}
