package com.wenbronk.shiro.oauth2.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author wenbronk
 * @Date 2019/10/28 11:19 上午
 * description:
 */
@Entity
@Table
@Data
public class Client {

    @Id
    private String id;

    private String name;
    private String clientId;
    private String clientSecurity;

}
