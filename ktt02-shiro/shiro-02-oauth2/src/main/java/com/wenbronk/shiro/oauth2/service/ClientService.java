package com.wenbronk.shiro.oauth2.service;

import com.wenbronk.shiro.oauth2.model.Client;
import com.wenbronk.shiro.oauth2.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author wenbronk
 * @Date 2019/10/28 11:22 上午
 * description:
 */
@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;


    public void save(Client client) {
        clientRepository.save(client);
    }

    public Client findById(String id) {
        return clientRepository.findById(id).get();
    }


}
