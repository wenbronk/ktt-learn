package com.wenbronk.shiro.oauth2.oauth.realm;

import com.wenbronk.shiro.oauth2.oauth.token.OAuht2Token;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @Author wenbronk
 * @Date 2019/10/29 9:12 上午
 * description:
 */
public class OAuth2Realm extends AbstractRealm {

    @Override
    public void setName(String name) {
        super.setName("OAuth2Realm");
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return token != null && OAuht2Token.class.isAssignableFrom(token.getClass());
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        return null;
    }


}
