package com.wenbronk.shiro.oauth2.oauth.config;

import com.google.common.collect.Lists;
import com.wenbronk.shiro.oauth2.oauth.filter.OAuth2AuthenticationFilter;
import com.wenbronk.shiro.oauth2.oauth.realm.PhoneIDRealm;
import com.wenbronk.shiro.oauth2.oauth.realm.UserNameRealm;
import com.wenbronk.shiro.oauth2.oauth.session.CustomSession;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.List;
import java.util.Map;

/**
 * @Author wenbronk
 * @Date 2019/10/28 3:52 下午
 * description:
 */
@Configuration
public class OAuthConfig {

    public static final String CRLF = "\r\n";

    @Bean
    public UserNameRealm getReal() {
        return new UserNameRealm();
    }

    @Bean
    public PhoneIDRealm getPhoneRealm() {
        return new PhoneIDRealm();
    }

    @Bean(name="cacheManager")
    public CacheManager createEhcacheManager() {
        EhCacheManager cacheManager = new EhCacheManager();
        String path = "classpath:ehcache.xml";
        cacheManager.setCacheManagerConfigFile(path);
        return cacheManager;
    }

    @Bean
    public DefaultWebSessionManager sessionManager(CacheManager cacheManager) {
        CustomSession customSession = new CustomSession();
        customSession.setCacheManager(cacheManager);
        return customSession;
    }

    @Bean
    public SecurityManager securityManager(UserNameRealm userNameRealm, PhoneIDRealm phoneIDRealm, CacheManager cacheManager, DefaultWebSessionManager customSession) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        List<Realm> realms = Lists.newArrayList(userNameRealm, phoneIDRealm);
        securityManager.setRealms(realms);
        securityManager.setSessionManager(customSession);
        securityManager.setCacheManager(cacheManager);
        return securityManager;
    }

    @Bean
    public ShiroFilterFactoryBean shirFilter(SecurityManager securityManager) {
        //1.创建shiro过滤器工厂
        ShiroFilterFactoryBean filterFactory = new ShiroFilterFactoryBean();
        //2.设置安全管理器
        filterFactory.setSecurityManager(securityManager);

        filterFactory.setFilters(createFilterChainMap(filterFactory));

        //3.通用配置（配置登录页面，登录成功页面，验证未成功页面）
        filterFactory.setLoginUrl("/autherror?code=1"); //设置登录页面
        filterFactory.setUnauthorizedUrl("/autherror?code=2"); //授权失败跳转页面

        //5.设置过滤器
        filterFactory.setFilterChainDefinitions(loadFilterChainDefinitions());

        return filterFactory;
    }

    public Map<String, Filter> createFilterChainMap(ShiroFilterFactoryBean filterFactory) {
        Map<String, Filter> filters = filterFactory.getFilters();
        OAuth2AuthenticationFilter oAuth2AuthenticationFilter = new OAuth2AuthenticationFilter();
//        oAuth2AuthenticationFilter.setAuthcCodeParam(OAuth.OAUTH_CODE);
//        oAuth2AuthenticationFilter.setResponseType(OAuth.OAUTH_CODE);
//        oAuth2AuthenticationFilter.setFailureUrl("/oauth2Failure");
        filters.put("oauth2Authc",oAuth2AuthenticationFilter);
        return filters;
    }

    //4.配置过滤器集合
    /**
     * key ：访问连接
     * 支持通配符的形式
     * value：过滤器类型
     * shiro常用过滤器
     * anno ：匿名访问（表明此链接所有人可以访问）
     * authc ：认证后访问（表明此链接需登录认证成功之后可以访问）
     *
     *         //配置请求连接过滤器配置
     *         //匿名访问（所有人员可以使用）
     * //        filterMap.put("/user/home", "anon");
     * ////具有指定权限访问
     * //        filterMap.put("/user/find", "perms[user-find]");
     * ////认证之后访问（登录之后可以访问）
     * //        filterMap.put("/user/**", "authc");
     * ////具有指定角色可以访问
     * //        filterMap.put("/user/**", "roles[系统管理员]");
     */
    public String loadFilterChainDefinitions() {
        StringBuffer sb = new StringBuffer();
        //	sb.append(getFixedAuthRule());//固定权限，采用读取配置文件
        sb.append("/user/logout = logout").append(CRLF);
        sb.append("/user/login = anon").append(CRLF);
        sb.append("/user/mobileLogin = anon").append(CRLF);
        sb.append("/user/** = authc").append(CRLF);
        sb.append("/user/oauth2Login = oauth2Authc").append(CRLF);
//        sb.append("/** = user").append(CRLF);

        return sb.toString();
    }

    //配置shiro注解支持
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }


}
