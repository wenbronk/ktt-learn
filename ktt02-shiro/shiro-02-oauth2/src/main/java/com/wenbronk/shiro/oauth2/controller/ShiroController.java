package com.wenbronk.shiro.oauth2.controller;

import com.wenbronk.shiro.oauth2.oauth.token.PhoneIdentiCodeToken;
import com.wenbronk.shiro.oauth2.service.OAuthService;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019/10/24 5:52 下午
 * description:
 */
@RestController
@RequestMapping("/user")
public class ShiroController {

    @Autowired
    private OAuthService oAuthService;

    /**
     *
     * @return
     */
    @RequestMapping("/login")
    public Object login(String userName, String password, String clientId, String security) throws OAuthSystemException {

        // 校验客户端id 和 security 是否正确
        if (!oAuthService.checkClientId(clientId, security)) {
            throw new RuntimeException("客户端验证失败，如错误的client_id/client_secret。");
        }

        // 登陆
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(userName, password);
        subject.login(usernamePasswordToken);
//        return subject.isAuthenticated();

        // 获取授权码， 目前只有CODE模式
//        ResponseType.TOKEN
        OAuthIssuerImpl oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
//        String authorizationCode = oauthIssuerImpl.authorizationCode();
        // 生成token， GrantType，密码模式
        final String accessToken = oauthIssuerImpl.accessToken();
        String refreshToken = oauthIssuerImpl.refreshToken();

//        oAuthService.addAuthCode(accessToken, username);

        return null;
    }

    @RequestMapping("/mobileLogin")
    public boolean mobileLogin(String phone, String identify) {
        Subject subject = SecurityUtils.getSubject();
        PhoneIdentiCodeToken phoneIdentiCodeToken = new PhoneIdentiCodeToken(phone);
        subject.login(phoneIdentiCodeToken);
        return subject.isAuthenticated();
    }

    @RequestMapping("/logout")
    public String logout(String userName) {
        System.out.println("logout userName: " + userName);
        return userName;
    }

    @RequestMapping("/findAll")
    public String findAll() {
        System.out.println("find All");
        return "find All";
    }

    @RequiresRoles(value = "root")
    @RequestMapping("/addUser")
    public String addUser() {
        System.out.println("add user");
        return "add User";
    }

    @RequiresPermissions("userDelete")
    @RequestMapping("/deleteUser")
    public String deleteUser() {
        System.out.println("delete user");
        return "delete User";
    }

    @RequiresPermissions("adminPermission")
    @RequestMapping("/admin")
    public String admin() {
        System.out.println("admin user");
        return "admin User";
    }

    @RequiresRoles("staff")
    @RequestMapping("/staff")
    public String staff() {
        System.out.println("staff user");
        return "staff User";
    }

}
