package com.wenbronk.shiro.oauth2.repository;

import com.wenbronk.shiro.oauth2.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Author wenbronk
 * @Date 2019/10/28 11:22 上午
 * description:
 */
public interface ClientRepository extends JpaRepository<Client, String> {

    Optional<Client> findByClientId(String s);

}
