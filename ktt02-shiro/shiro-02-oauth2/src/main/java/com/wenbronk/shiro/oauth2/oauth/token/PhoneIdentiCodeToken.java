package com.wenbronk.shiro.oauth2.oauth.token;

import lombok.Data;
import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

/**
 * @Author wenbronk
 * @Date 2019/10/25 11:08 上午
 * description:
 */
@Data
public class PhoneIdentiCodeToken implements HostAuthenticationToken, RememberMeAuthenticationToken {

    private String phone;

    private String identify;

    private boolean rememberMe = false;

    private String host;

    public PhoneIdentiCodeToken() {
    }

    public PhoneIdentiCodeToken(final String phone) {
        this(phone, null, false, null);
    }

    public PhoneIdentiCodeToken(final String phone, final String identify) {
        this(phone, identify, false, null);
    }

    public PhoneIdentiCodeToken(final String phone, final String identify, final String host) {
        this(phone, identify, false, host);
    }

    public PhoneIdentiCodeToken(final String phone, final String identify, final boolean rememberMe) {
        this(phone, identify, rememberMe, null);
    }

    public PhoneIdentiCodeToken(final String phone, final String identify,
                                final boolean rememberMe, final String host) {

        this.phone = phone;
        this.identify = identify;
        this.rememberMe = rememberMe;
        this.host = host;
    }

    public void clear() {
        this.phone = null;
        this.host = null;
        this.rememberMe = false;
        this.identify = null;
    }

    @Override
    public Object getPrincipal() {
        return getPhone();
    }

    @Override
    public Object getCredentials() {
        return identify;
    }
}
