package com.wenbronk.shiro.oauth2.test;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.junit.Test;

/**
 * @Author wenbronk
 * @Date 2019/10/29 9:21 上午
 * description:
 */
public class OAuth2Test {

    @Test
    public void testToken() throws Exception {
        OAuthIssuerImpl oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
        String authorizationCode = oauthIssuerImpl.authorizationCode();
        String accessToken = oauthIssuerImpl.accessToken();
        String refreshToken = oauthIssuerImpl.refreshToken();

        System.out.println(authorizationCode);
        System.out.println(accessToken);
        System.out.println(refreshToken);

        //d250aca25d6769e47172e2893c5f10d9
        //dd0336cc628ebf1e4c634ef0b1b59dd3
        //37eecd6466565b1aa9eca90e0e49be03
    }

    @Test
    public void testCheck() throws Exception {
        OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
        OAuthClientRequest accessTokenRequest = OAuthClientRequest
                .tokenLocation("dd0336cc628ebf1e4c634ef0b1b59dd3")
                .setGrantType(GrantType.AUTHORIZATION_CODE)
                .setClientId("app").setClientSecret("app_security")
                .setCode("d250aca25d6769e47172e2893c5f10d9")
                .setRedirectURI("localhost")
                .buildQueryMessage();
        //获取 access token
        OAuthAccessTokenResponse oAuthResponse =
                oAuthClient.accessToken(accessTokenRequest, OAuth.HttpMethod.POST);
        String accessToken = oAuthResponse.getAccessToken();


    }

}
