package com.wenbronk.shiro.oauth2.test;

import com.wenbronk.shiro.oauth2.Oauth2Main;
import com.wenbronk.shiro.oauth2.model.Client;
import com.wenbronk.shiro.oauth2.service.ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author wenbronk
 * @Date 2019/10/28 2:18 下午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Oauth2Main.class)
public class ClientServiceTest {

    @Autowired
    private ClientService clientService;

    @Test
    public void testAdd() {
        Client client = new Client();
        client.setId(Utils.getId());
        client.setName("手机端");
        client.setClientId("app");
        client.setClientSecurity("app_security");

        clientService.save(client);
    }

}
