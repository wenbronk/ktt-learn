dependencies {
    implementation(project(":ktt02-shiro:shiro-00-base"))

    implementation("org.apache.shiro:shiro-ehcache:1.3.2")
    implementation("org.apache.oltu.oauth2:org.apache.oltu.oauth2.authzserver:1.0.0")
    implementation("org.apache.oltu.oauth2:org.apache.oltu.oauth2.resourceserver:1.0.0")
    implementation("org.apache.oltu.oauth2:org.apache.oltu.oauth2.client:1.0.0")
    implementation("mysql:mysql-connector-java")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
}