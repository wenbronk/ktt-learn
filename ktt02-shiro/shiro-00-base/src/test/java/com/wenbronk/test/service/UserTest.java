package com.wenbronk.test.service;

import com.google.common.collect.Lists;
import com.wenbronk.shiro.base.ShiroMain;
import com.wenbronk.shiro.base.model.User;
import com.wenbronk.shiro.base.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:30 下午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShiroMain.class})
public class UserTest {

    @Autowired
    private UserService userService;

    @Test
    public void testSave() {
        User user = new User();
        user.setId(Utils.getId());
        user.setUsername("rootUser");
        user.setPassword("root");
        user.setPhone("110");
        userService.save(user);

        User user1 = new User();
        user1.setId(Utils.getId());
        user1.setUsername("adminUser");
        user1.setPassword("admin");
        user1.setPhone("111");
        userService.save(user1);

        User user2 = new User();
        user2.setId(Utils.getId());
        user2.setUsername("staffUser");
        user2.setPassword("staff");
        user2.setPhone("112");
        userService.save(user2);
    }

    @Test
    public void testAssignRootRole() {
        String userId = "f32834fbd96c44d3be22255e74401795";
        ArrayList<String> rolesIds = Lists.newArrayList(
                "c388565b364b475583b4148d3c7b706e",
                "3dd001cc967c45308d8b850360d62d4b",
                "59801a33503d4e81b9ecdac5150015cb");
        userService.assignRole2User(userId, rolesIds);
    }

    @Test
    public void testAssignAdminRole2() {
        String userId = "724f6a8de7ee4b2cbab2c120c9dbe874";
        ArrayList<String> rolesIds = Lists.newArrayList(
                "3dd001cc967c45308d8b850360d62d4b",
                "59801a33503d4e81b9ecdac5150015cb");
        userService.assignRole2User(userId, rolesIds);
    }

    @Test
    public void testAssignStaffRole3() {
        String userId = "24843d076a544fd5b6b643c49df869cb";
        ArrayList<String> rolesIds = Lists.newArrayList("59801a33503d4e81b9ecdac5150015cb");
        userService.assignRole2User(userId, rolesIds);
    }

}
