package com.wenbronk.test.service;

import java.util.UUID;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:55 下午
 * description:
 */
public class Utils {

    public static String getId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
