package com.wenbronk.test.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * @Author wenbronk
 * @Date 2019/10/24 5:16 下午
 * description:
 */
public class ShiroTest {

    /**
     * 用户名密码登陆
     */
    @Test
    public void testUserNameAndPassword() {
        //1.加载ini配置文件创建SecurityManager
        IniSecurityManagerFactory factory = new IniSecurityManagerFactory("classpath:shiro-auth.ini");

        //2.获取securityManager
        SecurityManager securityManager = factory.getInstance();

        //3.将securityManager绑定到当前运行环境
        SecurityUtils.setSecurityManager(securityManager);
        //4.创建主体(此时的主体还为经过认证)
        Subject subject = SecurityUtils.getSubject();

        /**
         * 模拟登录，和传统等不同的是需要使用主体进行登录
         */
        //5.构造主体登录的凭证（即用户名/密码）
        //第一个参数：登录用户名，第二个参数：登录密码
        UsernamePasswordToken upToken = new UsernamePasswordToken("zhangsan","123456");

        //6.主体登录
        subject.login(upToken);
        //7.验证是否登录成功
        System.out.println("用户登录成功="+subject.isAuthenticated());
        //8.登录成功获取数据
        //getPrincipal 获取登录成功的安全数据
        System.out.println(subject.getPrincipal());
    }

    /**
     * 授权
     */
    @Test
    public void testPermission() {
        IniSecurityManagerFactory iniSecurityManagerFactory = new IniSecurityManagerFactory("classpath:shiro-perm.ini");
        SecurityManager securityManager = iniSecurityManagerFactory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken upToken = new UsernamePasswordToken("zhangsan","123456");
        subject.login(upToken);

        // 坚权
        boolean permitted = subject.isPermitted("user:save");
        System.out.println(permitted);

    }

    /**
     * 通过 realm进行授权
     */
    @Test
    public void testRealm() {
        //1.加载ini配置文件创建SecurityManager
        IniSecurityManagerFactory factory = new IniSecurityManagerFactory("classpath:shiro-realm.ini");
        //2.获取securityManager
        SecurityManager securityManager = factory.getInstance();
        //13.将securityManager绑定到当前运行环境
        SecurityUtils.setSecurityManager(securityManager);

        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken upToken = new UsernamePasswordToken("lisi","123456");
        //3.主体登录
        subject.login(upToken);

        //登录成功验证是否具有role1角色
        //System.out.println("当前用户具有role1="+subject.hasRole("role3"));
        //登录成功验证是否具有某些权限
        System.out.println("当前用户具有user:save权限="+subject.isPermitted("user:save"));
    }


}
