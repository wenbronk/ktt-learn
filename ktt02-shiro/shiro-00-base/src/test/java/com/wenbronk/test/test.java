package com.wenbronk.test;

import org.junit.Test;

/**
 * @Author wenbronk
 * @Date 2019/10/25 8:39 下午
 * description:
 */
public class test {

    @Test
    public void test1() {
        System.out.println(count(5));
    }

    public int count(int n) {
        if (n < 0) {
            return 0;
        } else if (n == 0) {
            return 1;
        } else {
            int count = count(n - 1);
            int count2 = count(n - 2);
            int count3 = count(n - 3);
            return count + count2 + count3;
        }
    }

}
