package com.wenbronk.test.service;

import com.wenbronk.shiro.base.ShiroMain;
import com.wenbronk.shiro.base.model.Permission;
import com.wenbronk.shiro.base.model.Role;
import com.wenbronk.shiro.base.service.PermissionService;
import com.wenbronk.shiro.base.service.RoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:43 下午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ShiroMain.class})
public class RoleTest {

    @Autowired
    private RoleService roleService;

    @Autowired
    private PermissionService permissionService;

    @Test
    public void save() {
        Role role = new Role();
        role.setId(Utils.getId());
        role.setRoleName("root");
        roleService.save(role);

        Role role1 = new Role();
        role1.setId(Utils.getId());
        role1.setRoleName("admin");
        roleService.save(role1);

        Role role2 = new Role();
        role2.setId(Utils.getId());
        role2.setRoleName("staff");
        roleService.save(role2);
    }

    @Test
    public void testAsingPerm2Role() {
        asPerm2Role("root", "e6ff8f80c87249f09cdcf190c75398cc",
                "02abcf1cee534cada5eb667f9fcb8ac1",
                "1834084ddc1f4402bfd21db0b2ef3e4d",
                "5abd6b784c094feca7d73d7f249ab973");

        asPerm2Role("admin",
                "1834084ddc1f4402bfd21db0b2ef3e4d",
                "5abd6b784c094feca7d73d7f249ab973");

        asPerm2Role("staff",
                "5abd6b784c094feca7d73d7f249ab973");

    }

    public void asPerm2Role(String roleName, String... permissinIds) {
        Role role = roleService.findByRoleName(roleName);
        Set<Permission> permissions = Stream.of(permissinIds).map(permissionService::findById).collect(Collectors.toSet());
        role.setPermissions(permissions);
        roleService.save(role);
    }

}
