package com.wenbronk.shiro.base.repository;

import com.wenbronk.shiro.base.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:34 下午
 * description:
 */
public interface PermissionRepository extends JpaRepository<Permission, String> {

    Permission findByName(String name);

}
