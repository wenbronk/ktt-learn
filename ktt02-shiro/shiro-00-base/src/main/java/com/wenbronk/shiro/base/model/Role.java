package com.wenbronk.shiro.base.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:26 下午
 * description:
 */
@Data
@Entity
@Table
@DynamicInsert(true)
@DynamicUpdate(true)
@ToString(exclude = {"permission"})
public class Role  implements Serializable {
    @Id
    private String id;
    private String roleName;

//    @JsonIgnore
//    @ManyToMany(mappedBy = "roles")  //不维护中间表
//    private Set<User> users = new HashSet<User>(0);//角色与用户   多对多

    @Lazy
    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "pe_role_permission",
            joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "id")})
    private Set<Permission> permissions = new HashSet<Permission>(0);//角色与模块  多对多

}
