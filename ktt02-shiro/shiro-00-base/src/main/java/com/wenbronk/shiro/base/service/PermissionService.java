package com.wenbronk.shiro.base.service;

import com.wenbronk.shiro.base.model.Permission;
import com.wenbronk.shiro.base.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author wenbronk
 * @Date 2019/10/24 3:10 下午
 * description:
 */
@Service
public class PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    public void save(Permission permission) {
        permissionRepository.save(permission);
    }

    public Permission findByName(String name) {
        return permissionRepository.findByName(name);
    }

    public Permission findById(String id) {
        return permissionRepository.findById(id).get();
    }

}
