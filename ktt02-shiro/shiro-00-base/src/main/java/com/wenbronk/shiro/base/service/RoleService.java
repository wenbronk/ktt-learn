package com.wenbronk.shiro.base.service;

import com.wenbronk.shiro.base.model.Role;
import com.wenbronk.shiro.base.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:34 下午
 * description:
 */
@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public void save(Role role) {
        roleRepository.save(role);
    }

    public Role findByRoleName(String roleName) {
        return roleRepository.findByRoleName(roleName);
    }

}
