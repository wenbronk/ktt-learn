package com.wenbronk.shiro.base.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:02 下午
 * description:
 */
@Entity
@Table(name = "sys_user")
@Data
@DynamicInsert(true)
@DynamicUpdate(true)
@ToString(exclude = {"roles"})
public class User implements Serializable {

    private static final long serialVersionUID = 4297464181093070302L;
    /**
     * ID
     */
    @Id
    private String id;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 用户名称
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    @Lazy
    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "pe_user_role", joinColumns =
            {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")}
    )
    private Set<Role> roles = new HashSet<Role>();//用户与角色 多对多
}
