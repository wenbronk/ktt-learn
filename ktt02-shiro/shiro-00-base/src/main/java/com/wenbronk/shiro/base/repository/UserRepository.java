package com.wenbronk.shiro.base.repository;

import com.wenbronk.shiro.base.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:24 下午
 * description:
 */
public interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String name);

    User findByPhone(String phone);

}
