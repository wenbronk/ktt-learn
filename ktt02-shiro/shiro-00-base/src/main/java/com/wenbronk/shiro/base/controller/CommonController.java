package com.wenbronk.shiro.base.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019/10/25 8:56 上午
 * description:
 */
@RestController
public class CommonController {

    @RequestMapping("/autherror")
    public String autherror(String code) {
        System.out.println("error code is: " + code);
        return code;
    }

}
