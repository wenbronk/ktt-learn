package com.wenbronk.shiro.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author wenbronk
 * @Date 2019/10/31 10:53 下午
 * description:
 */
@SpringBootApplication
public class ShiroMain {
    public static void main(String[] args) {
        SpringApplication.run(ShiroMain.class, args);
    }
}
