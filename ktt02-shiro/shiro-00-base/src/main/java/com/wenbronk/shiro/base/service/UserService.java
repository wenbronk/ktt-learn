package com.wenbronk.shiro.base.service;

import com.wenbronk.shiro.base.model.Role;
import com.wenbronk.shiro.base.model.User;
import com.wenbronk.shiro.base.repository.RoleRepository;
import com.wenbronk.shiro.base.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:25 下午
 * description:
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public void save(User user) {
        userRepository.save(user);
    }

    public void deleteById(String id) {
        userRepository.deleteById(id);
    }

    public User findByUserName(String userName) {
        return userRepository.findByUsername(userName);
    }

    @Transactional(rollbackOn = Exception.class)
    public void assignRole2User(String userId, List<String> roleIds) {
        User user = userRepository.findById(userId).get();
        List<Role> allById = roleRepository.findAllById(roleIds);
        user.setRoles(new HashSet<>(allById));
        userRepository.save(user);
    }

    public User findByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }

}
