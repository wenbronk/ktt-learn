package com.wenbronk.shiro.base.repository;

import com.wenbronk.shiro.base.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author wenbronk
 * @Date 2019/10/24 2:34 下午
 * description:
 */
public interface RoleRepository extends JpaRepository<Role, String> {

    Role findByRoleName(String roleName);

}
