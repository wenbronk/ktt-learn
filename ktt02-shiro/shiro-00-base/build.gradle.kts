dependencies {
    compile("org.springframework.boot:spring-boot-starter-web")
    compile("org.springframework.boot:spring-boot-starter-data-jpa")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    compile("org.apache.shiro:shiro-spring:1.4.2")
    compile("org.apache.shiro:shiro-core:1.4.2")
    implementation("mysql:mysql-connector-java")
}