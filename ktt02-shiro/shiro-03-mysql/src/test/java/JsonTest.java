import com.alibaba.fastjson.JSON;
import org.apache.shiro.session.ProxiedSession;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;
import org.junit.Test;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/6 4:22 下午
 * description:
 */
public class JsonTest {

    @Test
    public void test() {
        String json = "{\"attributeKeys\":[\"org.apache.shiro.subject.support.DefaultSubjectContext_AUTHENTICATED_SESSION_KEY\",\"org.apache.shiro.subject.support.DefaultSubjectContext_PRINCIPALS_SESSION_KEY\"],\"host\":\"0:0:0:0:0:0:0:1\",\"id\":\"bda14244-99c5-4046-a21c-b58b934a817c\",\"lastAccessTime\":1575620097871,\"startTimestamp\":1575620097871,\"timeout\":-1}";
        Session session = JSON.parseObject(json, ProxiedSession.class);
        System.out.println(session);
    }

}
