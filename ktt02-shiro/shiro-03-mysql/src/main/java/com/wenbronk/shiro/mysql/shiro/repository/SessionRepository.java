package com.wenbronk.shiro.mysql.shiro.repository;

import com.wenbronk.shiro.mysql.shiro.model.SessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/6 11:23 上午
 * description:
 */
public interface SessionRepository extends JpaRepository<SessionEntity, Serializable> {
}
