package com.wenbronk.shiro.mysql.shiro.session;

import com.wenbronk.shiro.mysql.shiro.model.SessionEntity;
import com.wenbronk.shiro.mysql.shiro.repository.SessionRepository;
import com.wenbronk.shiro.mysql.utils.SerializeUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Optional;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/5 4:32 下午
 * description:
 */
@Repository
public class SqlSessionDao extends EnterpriseCacheSessionDAO {

    @Autowired
    private SessionRepository sessionRepository;

    @Override
    public void setSessionIdGenerator(SessionIdGenerator sessionIdGenerator) {
        super.setSessionIdGenerator(sessionIdGenerator);
    }

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = super.doCreate(session);
        assignSessionId(session, sessionId);
        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setId((String) sessionId);
        sessionEntity.setSession(SerializeUtils.serialize(session));
        sessionRepository.save(sessionEntity);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        Session session = super.doReadSession(sessionId);

        if (session == null) {
            Optional<SessionEntity> sessionEntityOptional = sessionRepository.findById((String)sessionId);
            if (sessionEntityOptional.isPresent()) {
                session = (Session) SerializeUtils.deserialize(sessionEntityOptional.get().getSession());
                //如果库有，缓存却没有，加入缓存，readsession调用次数太多
                //super.doCreate(session);虽然可以加入缓存，但错误的 会产生新的seesionid
                cache(session, sessionId);
            }
        }
                                                                                    return session;

    }

    //更新session .shiro对每一次请求都会更新最后访问时间.当一个页面包含多个资源的时候就会发生多次update session
    @Override
    protected void doUpdate(Session session) {
        super.doUpdate(session);
        SessionEntity userSession = new SessionEntity();
        userSession.setSession(SerializeUtils.serialize(session));
        userSession.setId((String) session.getId());
        //getSecurityManager()是ShiroUtils.getUser()底层的方法，未登录的情况下SecurityManager不存在，就报错
//        if (ThreadContext.getSecurityManager() != null && ShiroUtils.getUser() != null) {
//            userSession.setDlUserCode(ShiroUtils.getUser().getCode());
//        }
        sessionRepository.save(userSession);
    }

    @Override
    protected void doDelete(Session session) {
        super.doDelete(session);
        if (session == null || session.getId() == null) {
            return;
        }
        sessionRepository.deleteById(session.getId());
    }
}
