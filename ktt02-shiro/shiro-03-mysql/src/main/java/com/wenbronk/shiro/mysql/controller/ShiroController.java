package com.wenbronk.shiro.mysql.controller;

import com.wenbronk.shiro.mysql.shiro.session.SqlSessionDao;
import com.wenbronk.shiro.mysql.shiro.token.PhoneIdentiCodeToken;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019/10/24 5:52 下午
 * description:
 */
@RestController
@RequestMapping("/user")
public class ShiroController {

    @Autowired
    private SqlSessionDao sessionDao;

    /**
     *
     * @return
     */
    @RequestMapping("/login")
    public boolean login(String userName, String password) {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(userName, password);
        subject.login(usernamePasswordToken);

        // 登陆成功的时候， 保存session
//        Session session = subject.getSession();
//        sessionDao.saveOnLogin("12341234", session);

        return subject.isAuthenticated();
    }

    @RequestMapping("/mobileLogin")
    public boolean mobileLogin(String phone, String identify) {
        Subject subject = SecurityUtils.getSubject();
        PhoneIdentiCodeToken phoneIdentiCodeToken = new PhoneIdentiCodeToken(phone);
        subject.login(phoneIdentiCodeToken);
        return subject.isAuthenticated();
    }

    @RequestMapping("/logout")
    public String logout(String userName) {
        System.out.println("logout userName: " + userName);
        return userName;
    }

    @RequestMapping("/findAll")
    public String findAll() {
        System.out.println("find All");
        return "find All";
    }

    @RequiresRoles(value = "root")
    @RequestMapping("/addUser")
    public String addUser() {
        System.out.println("add user");
        return "add User";
    }

    @RequiresPermissions("userDelete")
    @RequestMapping("/deleteUser")
    public String deleteUser() {
        System.out.println("delete user");
        return "delete User";
    }

    @RequiresPermissions("adminPermission")
    @RequestMapping("/admin")
    public String admin() {
        System.out.println("admin user");
        return "admin User";
    }

    @RequiresRoles("staff")
    @RequestMapping("/staff")
    public String staff() {
        System.out.println("staff user");
        return "staff User";
    }

}
