//package com.wenbronk.shiro.mysql.shiro.model;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.google.common.collect.Maps;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.NoArgsConstructor;
//import org.apache.shiro.session.InvalidSessionException;
//import org.apache.shiro.session.Session;
//import org.apache.shiro.session.mgt.SimpleSession;
//
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import java.io.Serializable;
//import java.util.Collection;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// * @author: wenbronk <wenbronk@163.com>
// * @date: 2019/12/5 4:37 下午
// * description:
// */
//@Data
//@Entity
//@Table
//@NoArgsConstructor
//@EqualsAndHashCode(callSuper = false)
//public class SessionEntity extends SimpleSession implements Session, Serializable {
//    public static final long serialVersionUID = 100000L;
//
//    @Id
//    private String id;
//
//    private Date startTimestamp;
//    private Date lastAccessTime;
//    private long timeout;
//    private String host;
//    private JSONObject attributes;
//
//    public SessionEntity(Session session) {
//        this.id = (String) session.getId();
//        this.host = session.getHost();
//        this.startTimestamp = session.getStartTimestamp();
//        this.lastAccessTime = session.getLastAccessTime();
//        setAttribute(session);
//    }
//
//    public void setAttribute(Session session) {
//        Collection<Object> attributeKeys = session.getAttributeKeys();
//        if (attributeKeys != null && attributeKeys.size() > 0) {
//            if (attributes == null) {
//                attributes = new JSONObject();
//            }
//            for (Object key : attributeKeys) {
//                attributes.put(JSON.toJSONString(key), session.getAttribute(key));
//            }
//        }
//    }
//
//    @Override
//    public void touch() throws InvalidSessionException {
//        this.lastAccessTime = new Date();
//    }
//
//    @Override
//    public void stop() throws InvalidSessionException {
//        // TODO
//
//    }
//
//    @Override
//    public Collection<Object> getAttributeKeys() throws InvalidSessionException {
//        return attributes.keySet().stream().map(JSON::parse).collect(Collectors.toSet());
//    }
//
//    @Override
//    public Object getAttribute(Object key) throws InvalidSessionException {
//        if (this.attributes == null) {
//            return null;
//        }
//        return this.attributes.get(JSON.toJSONString(key));
//    }
//
//    @Override
//    public void setAttribute(Object key, Object value) throws InvalidSessionException {
//        this.attributes.put(JSON.toJSONString(key), value);
//    }
//
//    @Override
//    public Object removeAttribute(Object key) throws InvalidSessionException {
//        return attributes.remove(JSON.toJSONString(key));
//    }
//
//    @Override
//    public Map<Object, Object> getAttributes() {
//        HashMap<Object, Object> objectObjectHashMap = Maps.newHashMap();
//        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
//            objectObjectHashMap.put(JSON.parse(entry.getKey()), entry.getValue());
//        }
//        return objectObjectHashMap;
//    }
//
//    @Override
//    public void setAttributes(Map<Object, Object> attributesMap) {
//        if (this.attributes == null) {
//            this.attributes = new JSONObject();
//        }
//        for (Map.Entry<Object, Object> entry : attributesMap.entrySet()) {
//            this.attributes.put(JSON.toJSONString(entry.getKey()), entry.getValue());
//        }
//    }
//}
