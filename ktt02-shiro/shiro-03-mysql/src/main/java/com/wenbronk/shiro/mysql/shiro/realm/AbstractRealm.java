package com.wenbronk.shiro.mysql.shiro.realm;


import com.wenbronk.shiro.base.model.Permission;
import com.wenbronk.shiro.base.model.Role;
import com.wenbronk.shiro.base.model.User;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * @Author wenbronk
 * @Date 2019/10/25 10:21 上午
 * description:
 */
public abstract class AbstractRealm extends AuthorizingRealm {

    /**
     * 坚权
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // 存进去的是UserUser， 所以取出来是User
        User user = (User) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        // 获取角色， 权限
        for (Role role : user.getRoles()) {
            simpleAuthorizationInfo.addRole(role.getRoleName());

            for (Permission permission : role.getPermissions()) {
                simpleAuthorizationInfo.addStringPermission(permission.getName());
            }
        }
        return simpleAuthorizationInfo;
    }

}
