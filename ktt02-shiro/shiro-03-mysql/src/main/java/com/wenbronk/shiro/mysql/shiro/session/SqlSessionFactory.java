package com.wenbronk.shiro.mysql.shiro.session;

import com.wenbronk.shiro.mysql.shiro.model.SessionEntity;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SimpleSessionFactory;
import org.springframework.stereotype.Component;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/6 4:45 下午
 * description:
 */
//@Component
//public class SqlSessionFactory extends SimpleSessionFactory {
//
//    @Override
//    public Session createSession(SessionContext initData) {
//        if (initData != null) {
//            String host = initData.getHost();
//            if (host != null) {
//                SessionEntity sessionEntity = new SessionEntity();
//                sessionEntity.setHost(host);
//                return sessionEntity;
//            }
//        }
//        return new SessionEntity();
//    }
//}
