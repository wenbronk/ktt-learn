package com.wenbronk.shiro.mysql.shiro.realm;

import com.wenbronk.shiro.base.model.User;
import com.wenbronk.shiro.base.service.UserService;
import org.apache.shiro.authc.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author wenbronk
 * @Date 2019/10/24 5:55 下午
 * description:
 */
public class UserNameRealm extends AbstractRealm {

    @Autowired
    private UserService userService;

    @Override
    public void setName(String name) {
        super.setName("customRealm");
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        String username = usernamePasswordToken.getUsername();
        String passwd = new String(usernamePasswordToken.getPassword());

        User user = userService.findByUserName(username);

        //4.用户存在并且密码匹配存储用户数据
        if(user != null && user.getPassword().equals(passwd)) {
            return new SimpleAuthenticationInfo(user,user.getPassword(),this.getName());
        }else {
        //返回null会抛出异常，表明用户不存在或密码不匹配
            return null;
        }
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return token != null && UsernamePasswordToken.class.isAssignableFrom(token.getClass());
    }
}
