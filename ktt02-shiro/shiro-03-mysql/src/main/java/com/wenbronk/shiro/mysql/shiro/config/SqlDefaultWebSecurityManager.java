package com.wenbronk.shiro.mysql.shiro.config;

import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.subject.SubjectContext;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.WebSessionKey;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/6 2:13 下午
 * description:
 */
//public class SqlDefaultWebSecurityManager extends DefaultWebSecurityManager {
//
//    @Override
//    protected SessionKey getSessionKey(SubjectContext context) {
//        SessionKey sessionKey = super.getSessionKey(context);
//        if (sessionKey != null && sessionKey instanceof WebSessionKey) {
//            WebSessionKey webSessionKey = (WebSessionKey) sessionKey;
//            HttpServletRequest servletRequest = (HttpServletRequest) webSessionKey.getServletRequest();
//            String accessToken = servletRequest.getHeader("access_token");
////            String  = String.valueOf(servletRequest.getAttribute(""));
//            if (!StringUtils.isEmpty(accessToken)) {
//                webSessionKey.setSessionId(accessToken);
//            }
//        }
//        return sessionKey;
//    }
//}
