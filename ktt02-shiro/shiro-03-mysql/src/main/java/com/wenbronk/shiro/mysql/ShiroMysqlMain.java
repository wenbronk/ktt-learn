package com.wenbronk.shiro.mysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/5 4:02 下午
 * description:
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wenbronk.shiro.base", "com.wenbronk.shiro.mysql"})
@EntityScan(basePackages = {"com.wenbronk.shiro.base", "com.wenbronk.shiro.mysql"})
public class ShiroMysqlMain {
    public static void main(String[] args) {
        SpringApplication.run(ShiroMysqlMain.class, args);
    }
}
