package com.wenbronk.shiro.mysql.shiro.model;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/5 4:37 下午
 * description:
 */
@Data
@Entity
@Table
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SessionEntity implements Serializable {
    public static final long serialVersionUID = 100000L;

    @Id
    private String id;

    @Column(length = 9000)
    private String session;

}
