package com.wenbronk.shiro.mysql.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;

import java.io.*;
import java.util.HashMap;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/6 11:21 上午
 * description:
 */
public class SerializeUtils {

//    public static Object parse(String json) {
//        JSONObject object = JSON.parseObject(json);
//        SimpleSession simpleSession = new SimpleSession();
//        if (object.getString("id") != null) {
//            simpleSession.setId(object.getString("id"));
//        }
//        if (object.getString("host") != null) {
//            simpleSession.setHost(object.getString("host"));
//        }
//        if (object.getLong("timeout") != null) {
//            simpleSession.setTimeout(object.getLong("timeout"));
//        }
//        if (object.getDate("stopTimestamp") != null) {
//            simpleSession.setStopTimestamp(object.getDate("stopTimestamp"));
//        }
//        if (object.getDate("startTimestamp") != null) {
//            simpleSession.setStartTimestamp(object.getDate("startTimestamp"));
//        }
//        if (object.getDate("lastAccessTime") != null) {
//            simpleSession.setLastAccessTime(object.getDate("lastAccessTime"));
//        }
//        if (object.getJSONArray("attributeKeys") != null && object.getJSONArray("attributeKeys").size() > 0) {
//            HashMap<Object, Object> objectObjectHashMap = Maps.newHashMap();
//            for (Object key : object.getJSONArray("attributeKeys")) {
//
//            }
//            simpleSession.setAttributes();
//        }
//        return simpleSession;
//    }

//    public static String toStr(Object obj) {
//        JSONObject json = JSON.parseObject(JSON.toJSONString(obj));
//        return json.toJSONString();
//    }


//    /**
//     * deserialize
//     *
//     * @param bytes
//     * @return
//     */
//    public static Object deserialize(byte[] bytes) {
//        Object result = null;
//        if (isEmpty(bytes)) {
//            return null;
//        }
//        try {
//            ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
//            try {
//                ObjectInputStream objectInputStream = new ObjectInputStream(byteStream);
//                try {
//                    result = objectInputStream.readObject();
//                } catch (ClassNotFoundException ex) {
//                    throw new Exception("Failed to deserialize object type", ex);
//                }
//            } catch (Throwable ex) {
//                throw new Exception("Failed to deserialize", ex);
//            }
//        } catch (Exception e) {
////            log.error("Failed to deserialize", e);
//        }
//        return result;
//    }
//
//    public static boolean isEmpty(byte[] data) {
//        return data == null || data.length == 0;
//    }
//
//    /**
//     * serialize
//     *
//     * @param object
//     * @return
//     */
//    public static byte[] serialize(Object object) {
//        byte[] result = null;
//        if (object == null) {
//            return new byte[0];
//        }
//        try {
//            ByteArrayOutputStream byteStream = new ByteArrayOutputStream(128);
//            try {
//                if (!(object instanceof Serializable)) {
//                    throw new IllegalArgumentException(
//                            SerializeUtils.class.getSimpleName() + " requires a Serializable payload "
//                                    + "but received an object of type [" + object.getClass().getName() + "]");
//                }
//                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteStream);
//                objectOutputStream.writeObject(object);
//                objectOutputStream.flush();
//                result = byteStream.toByteArray();
//            } catch (Throwable ex) {
//                throw new Exception("Failed to serialize", ex);
//            }
//        } catch (Exception ex) {
////            log.error("Failed to serialize", ex);
//            ex.printStackTrace();
//        }
//        return result;
//    }

    /**
     * deserialize
     */
    public static Session deserialize(String sessionStr) {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(Base64.decode(sessionStr));
            ObjectInputStream ois = new ObjectInputStream(bis);
            return (Session)ois.readObject();
        } catch (Exception e) {
            throw new RuntimeException("deserialize session error", e);
        }
    }

    /**
     * serialize
     */
    public static String serialize(Session session) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(session);
            return Base64.encodeToString(bos.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException("serialize session error", e);
        }
    }

}
