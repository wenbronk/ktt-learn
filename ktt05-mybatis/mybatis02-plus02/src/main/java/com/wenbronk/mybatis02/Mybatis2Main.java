package com.wenbronk.mybatis02;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2020/1/2 2:06 下午
 * description:
 */
@SpringBootApplication
@MapperScan("com.wenbronk.mybatis02.mapper")
public class Mybatis2Main {
    public static void main(String[] args) {
        SpringApplication.run(Mybatis2Main.class, args);
    }
}
