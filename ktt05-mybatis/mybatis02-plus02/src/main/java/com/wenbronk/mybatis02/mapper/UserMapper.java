package com.wenbronk.mybatis02.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wenbronk.mybatis02.model.User;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2020/1/2 2:10 下午
 * description:
 */
public interface UserMapper extends BaseMapper<User> {
}
