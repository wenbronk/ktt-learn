package com.wenbronk.mybatis02.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2020/1/2 2:08 下午
 * description:
 */
@Data
//@TableName(value = "user", autoResultMap = true)
public class User {

//    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

//    @TableField(value = "name", exist = true)
    private String name;
    private Integer age;
    private String email;

//    @Version
//    private Long version;

    //    @TableLogic(value = "0", delval = "1")
//    private Integer deleteTag;

//    private Long tenentId;


}
