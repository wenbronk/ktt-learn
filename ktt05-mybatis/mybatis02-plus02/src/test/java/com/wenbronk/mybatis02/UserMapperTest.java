package com.wenbronk.mybatis02;

import com.wenbronk.mybatis02.mapper.UserMapper;
import com.wenbronk.mybatis02.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2020/1/2 2:11 下午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testQuery() {
        List<User> users = userMapper.selectList(null);
        System.out.println(users.size());
    }

}
