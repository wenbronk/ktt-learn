dependencies {
    implementation("mysql:mysql-connector-java")
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.baomidou:mybatis-plus-boot-starter:3.3.0")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}