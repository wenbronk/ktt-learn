package com.wenbronk.mybatis01.test.mapper;

import com.wenbronk.mybatis01.mapper.UserMapper;
import com.wenbronk.mybatis01.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/18 9:44 上午
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelect() {
        List<User> users = userMapper.selectList(null);
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Test
    public void testSave() {
        User user = new User();
        user.setName("vini");
        user.setEmail("wenbronk@163.com");
        userMapper.insert(user);
    }

    @Test
    public void testSoftDelete() {
        int i = userMapper.deleteById(1);
        System.out.println(i);
    }

}
