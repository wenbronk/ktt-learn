package com.wenbronk.mybatis01;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/18 9:40 上午
 * description:
 */
@SpringBootApplication
@MapperScan("com.wenbronk.mybatis01.mapper")
public class BaseMain {
    public static void main(String[] args) {
        SpringApplication.run(BaseMain.class, args);
    }
}
