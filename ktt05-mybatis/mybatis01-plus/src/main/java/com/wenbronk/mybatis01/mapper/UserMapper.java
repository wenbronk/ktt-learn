package com.wenbronk.mybatis01.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wenbronk.mybatis01.model.User;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/18 9:42 上午
 * description:
 */
@SqlParser(filter = false)
public interface UserMapper extends BaseMapper<User> {
}
