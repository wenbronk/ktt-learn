package com.wenbronk.mybatis01.config;

import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * @author: wenbronk <wenbronk@163.com>
 * @date: 2019/12/18 4:19 下午
 * description:
 */
public class MybatisIKeyGenerator implements IKeyGenerator {
    @Override
    public String executeSql(String incrementerName) {
        return "select " + IdWorker.getId() + "from dual";
    }
}
